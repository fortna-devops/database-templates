import boto3
import json
from _datetime import datetime


STACK = 'dev'
DOMAIN = 'mhsinsights.com'
AUTH_VERSION = '77'


def main():
    s3 = boto3.client("s3")
    s3.create_bucket(Bucket='mhspredict-front' + '-' + STACK)
    s3.put_bucket_encryption(
        Bucket='mhspredict-front' + '-' + STACK,
        ServerSideEncryptionConfiguration={
            'Rules': [
                {
                    'ApplyServerSideEncryptionByDefault': {
                        'SSEAlgorithm': 'AES256'
                    }
                }
            ]
        }
    )
    cloudfront = boto3.client('cloudfront')
    response = cloudfront.create_distribution(
        DistributionConfig={
            'CallerReference': datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f'),
            'Aliases': {
                'Quantity': 1,
                'Items': [STACK + '.' + DOMAIN]
            },
            'DefaultRootObject': 'index.html',
            'Origins': {
                'Quantity': 2,
                'Items': [
                    {
                        'Id': 'S3-mhspredict-front' + '-' + STACK,
                        'DomainName': 'mhspredict-front' + '-' + STACK +'.s3.amazonaws.com',
                        'OriginPath': '',
                        'CustomHeaders': {
                            'Quantity': 0
                        },
                        'S3OriginConfig': {
                            'OriginAccessIdentity': 'origin-access-identity/cloudfront/E2PHG39PLVV44W'
                        }
                    },
                    {
                        'Id': 'S3-mhspredict-assets',
                        'DomainName': 'mhspredict-assets.s3.amazonaws.com',
                        'OriginPath': '',
                        'CustomHeaders': {
                            'Quantity': 0
                        },
                        'S3OriginConfig': {
                            'OriginAccessIdentity': 'origin-access-identity/cloudfront/E2PHG39PLVV44W'
                        }
                    }
                ]
            },
            'DefaultCacheBehavior': {
                'TargetOriginId': 'S3-mhspredict-front' + '-' + STACK,
                'ForwardedValues': {
                    'QueryString': False,
                    'Cookies': {
                        'Forward': 'none'
                    },
                    'Headers': {
                        'Quantity': 0
                    },
                    'QueryStringCacheKeys': {
                        'Quantity': 0
                    }
                },
                'TrustedSigners': {
                    'Enabled': False,
                    'Quantity': 0
                },
                'ViewerProtocolPolicy': 'redirect-to-https',
                'MinTTL': 0,
                'AllowedMethods': {
                    'Quantity': 2,
                    'Items': ['HEAD', 'GET'],
                    'CachedMethods': {
                        'Quantity': 2,
                        'Items': ['HEAD', 'GET']
                    }
                },
                'SmoothStreaming': False,
                'DefaultTTL': 86400,
                'MaxTTL': 31536000,
                'Compress': False,
                'LambdaFunctionAssociations': {
                    'Quantity': 2,
                    'Items': [
                        {
                            'LambdaFunctionARN': 'arn:aws:lambda:us-east-1:286214959794:function:auth-proxy:' + AUTH_VERSION,
                            'EventType': 'viewer-request',
                            'IncludeBody': False
                        },
                        {
                            'LambdaFunctionARN': 'arn:aws:lambda:us-east-1:286214959794:function:security-headers:3',
                            'EventType': 'origin-response',
                            'IncludeBody': False
                        }
                    ]
                },
                'FieldLevelEncryptionId': ''
            },
            'CacheBehaviors': {
                'Quantity': 1,
                'Items': [
                    {
                        'PathPattern': '/external/*',
                        'TargetOriginId': 'S3-mhspredict-assets',
                        'ForwardedValues': {
                            'QueryString': False,
                            'Cookies': {
                                'Forward': 'none'
                            },
                            'Headers': {
                                'Quantity': 0
                            },
                            'QueryStringCacheKeys': {
                                'Quantity': 0
                            }
                        },
                        'TrustedSigners': {
                            'Enabled': False,
                            'Quantity': 0
                        },
                        'ViewerProtocolPolicy': 'redirect-to-https',
                        'MinTTL': 0,
                        'AllowedMethods': {
                            'Quantity': 2,
                            'Items': ['HEAD', 'GET'],
                            'CachedMethods': {
                                'Quantity': 2,
                                'Items': ['HEAD', 'GET']
                            }
                        },
                        'SmoothStreaming': False,
                        'DefaultTTL': 86400,
                        'MaxTTL': 31536000,
                        'Compress': False,
                        'LambdaFunctionAssociations': {
                            'Quantity': 2,
                            'Items': [
                                {
                                    'LambdaFunctionARN': 'arn:aws:lambda:us-east-1:286214959794:function:auth-proxy:' + AUTH_VERSION,
                                    'EventType': 'viewer-request',
                                    'IncludeBody': False
                                },
                                {
                                    'LambdaFunctionARN': 'arn:aws:lambda:us-east-1:286214959794:function:security-headers:3',
                                    'EventType': 'origin-response',
                                    'IncludeBody': False
                                }
                            ]
                        },
                        'FieldLevelEncryptionId': ''
                    }
                ]
            },
            'CustomErrorResponses': {
                'Quantity': 1,
                'Items': [
                    {
                        'ErrorCode': 404,
                        'ResponsePagePath': '/index.html',
                        'ResponseCode': '200',
                        'ErrorCachingMinTTL': 300
                    }
                ]
            },
            'Comment': 'Backend stack for ' + STACK + ' environment',
            'Logging': {
                'Enabled': False,
                'IncludeCookies': False,
                'Bucket': '',
                'Prefix': ''
            },
            'PriceClass': 'PriceClass_100',
            'Enabled': True,
            'ViewerCertificate': {
                'ACMCertificateArn': 'arn:aws:acm:us-east-1:286214959794:certificate/96a1e192-0060-411e-bfea-31b45d36f5f5',
                'SSLSupportMethod': 'sni-only',
                'MinimumProtocolVersion': 'TLSv1.1_2016',
                'Certificate': 'arn:aws:acm:us-east-1:286214959794:certificate/96a1e192-0060-411e-bfea-31b45d36f5f5',
                'CertificateSource': 'acm'
            },
            'Restrictions': {
                'GeoRestriction': {
                    'RestrictionType': 'none',
                    'Quantity': 0
                }
            },
            'WebACLId': '',
            'HttpVersion': 'http2',
            'IsIPV6Enabled': True
        }
    )

    #response = client.get_distribution(Id='E2N3TCBRDYSRKO')
    #response = client.get_distribution(Id='EQ4NNLYQVOTKC')
    print(response)

    cfdn = response['Distribution']['DomainName']

    response = s3.put_bucket_policy(
        Bucket='mhspredict-front' + '-' + STACK,
        Policy=json.dumps({
            "Version": "2008-10-17",
            "Id": "PolicyForCloudFrontPrivateContent",
            "Statement": [
                {
                    "Sid": "1",
                    "Effect": "Allow",
                    "Principal": {
                        "AWS": "arn:aws:iam::cloudfront:user/CloudFront Origin Access Identity E2PHG39PLVV44W"
                    },
                    "Action": "s3:GetObject",
                    "Resource": "arn:aws:s3:::mhspredict-front-" + STACK + "/*"
                },
                {
                    "Sid": "2",
                    "Effect": "Allow",
                    "Principal": {
                        "AWS": "arn:aws:iam::cloudfront:user/CloudFront Origin Access Identity E2PHG39PLVV44W"
                    },
                    "Action": "s3:ListBucket",
                    "Resource": "arn:aws:s3:::mhspredict-front-" + STACK
                }
            ]
        })
    )

    r53 = boto3.client('route53')
    response = r53.change_resource_record_sets(
        HostedZoneId='Z16S1J0HIHHN9B',
        ChangeBatch={
            'Comment': 'CloudFront distribution',
            'Changes': [
                {
                    'Action': 'CREATE',
                    'ResourceRecordSet': {
                        'Name': STACK + '.mhsinsights.com.',
                        'Type': 'A',
                        'AliasTarget': {
                            'HostedZoneId': 'Z2FDTNDATAQYW2',
                            'DNSName': cfdn,
                            'EvaluateTargetHealth': False
                        }
                    }
                },
                {
                    'Action': 'CREATE',
                    'ResourceRecordSet': {
                        'Name': STACK + '.mhsinsights.com.',
                        'Type': 'AAAA',
                        'AliasTarget': {
                            'HostedZoneId': 'Z2FDTNDATAQYW2',
                            'DNSName': cfdn,
                            'EvaluateTargetHealth': False
                        }
                    }
                }
            ]
        }
    )
    print(response)


if __name__ == "__main__":
    main()
