import boto3

SITE = 'diverter'
BUCKET = 'mhspredict-site-' + SITE
PREFIX = 'dev'
s3_input = ''
s3_output = "s3://" + BUCKET + "/" + PREFIX + "/tmp"
database_name = "`" + BUCKET + "`"
table_name = "stats"
location = "s3://" + BUCKET + "/" + PREFIX + "/stats"

mapping = {
    'sensor' : 'sensor',
    'equipment': 'equipment',
    'standards_score': 'standards_score',
    'temp_score': 'temp_score',
    'vvrms_score': 'vvrms_score',
    'ac_score': 'ac_score',
    'conveyor': 'conveyor',
    'm_sensor': 'm_sensor',
    'description': 'description',
    'invalid_plc_metrics':'invalid_plc_metrics',
    'location':'location',
    'crest_acceleration_x': 'X_CREST',
    'crest_acceleration_z': 'Z_CREST',
    'hf_rms_acceleration_x': 'X_HF_RMS_G',
    'hf_rms_acceleration_z': 'Z_HF_RMS_G',
    'kurtosis_x': 'X_KURT',
    'kurtosis_z': 'Z_KURT',
    'peak_acceleration_x': 'X_PEAK_G',
    'peak_acceleration_z': 'Z_PEAK_G',
    'peak_frequency_x': 'X_PEAK_V_HZ',
    'peak_frequency_z': 'Z_PEAK_V_HZ',
    'peak_velocity_x': 'X_PEAK_V_IPS',
    'peak_velocity_z': 'Z_PEAK_V_IPS',
    'rms_acceleration_x': 'X_RMS_G',
    'rms_acceleration_z': 'Z_RMS_G',
    'rms_velocity_x': 'X_RMS_IPS',
    'rms_velocity_z': 'Z_RMS_IPS',
    'temperature': 'TEMP_F'

}

mapping_plc ={
    'vfd_voltage' : 'motor_voltage',
    'belt_speed': 'belt_speed',
    'sorter_induct_rate': 'induct_rate',
    'vfd_frequency': 'motor_freq',
    'sorter_reject_rate':'reject_rate',
    'vfd_current':'motor_current',
    'vfd_fault_code': 'vfd_fault_code',
    'status': 'conveyor_status',
    'gateway': 'gateway',
    'plc_score':'plc_score',
    'plc_time' : 'plc_time',
    'port': 'port',
    'timestamp': 'read_time'
}

metrics=['TEMP_F','X_CREST','X_HF_RMS_G','X_KURT','X_PEAK_G','X_PEAK_V_HZ', \
         'X_PEAK_V_IPS','X_RMS_G','X_RMS_V_IPS', \
         'Z_CREST','Z_HF_RMS_G','Z_KURT','Z_PEAK_G','Z_PEAK_V_HZ', \
         'Z_PEAK_V_IPS','Z_RMS_G','Z_RMS_IPS']
plc_metrics=['belt_speed','motor_voltage','induct_rate','motor_freq','reject_rate','motor_current','vfd_fault_code']


additional_fields = {}

for m in metrics:
    for athena_key, dynamo_key in mapping.items():
        if m == dynamo_key:
            additional_fields[athena_key+ '_thr_Y'] = m+ '_thr_Y'
            additional_fields[athena_key + '_thr_R'] = m+ '_thr_R'
            additional_fields[athena_key+'_mean']=m+'_mean'
            additional_fields[athena_key+'_std']=m+'_std'
            additional_fields[athena_key+'_median']=m+'_median'
            additional_fields[athena_key+'_mode']=m+'_mode'
            additional_fields[athena_key+'_min']=m+'_min'
            additional_fields[athena_key+'_max']=m+'_max'
            additional_fields[athena_key+'_perc25']=m+'_perc25'
            additional_fields[athena_key+'_perc75']=m+'_perc75'
            additional_fields[athena_key+'_perc05']=m+'_perc05'
            additional_fields[athena_key+'_perc95']=m+'_perc95'




additional_fields_plc = {}
for m in plc_metrics:
    for athena_key, dynamo_key in mapping_plc.items():
        if m == dynamo_key:
            additional_fields_plc[m+ '_thr_Y'] = m+ '_thr_Y'
            additional_fields_plc[m + '_thr_R'] = m+ '_thr_R'
            additional_fields_plc[m+'_mean']=m+'_mean'
            additional_fields_plc[m+'_std']=m+'_std'
            additional_fields_plc[m+'_median']=m+'_median'
            additional_fields_plc[m+'_mode']=m+'_mode'
            additional_fields_plc[m+'_min']=m+'_min'
            additional_fields_plc[m+'_max']=m+'_max'
            additional_fields_plc[m+'_perc25']=m+'_perc25'
            additional_fields_plc[m+'_perc75']=m+'_perc75'
            additional_fields_plc[m+'_perc05']=m+'_perc05'
            additional_fields_plc[m+'_perc95']=m+'_perc95'

final_keys = {**mapping, **mapping_plc , **additional_fields , **additional_fields_plc}
keys = final_keys.values()
headers = final_keys.keys()


def run_query(query, database, s3_output):
    client = boto3.client('athena')
    response = client.start_query_execution(
        QueryString=query,
        QueryExecutionContext={
            'Database': database
        },
        ResultConfiguration={
            'OutputLocation': s3_output,
        }
    )
    print('Execution ID: ' + response['QueryExecutionId'])
    return response


def main(database, table, s3_output):
    f = open("stats_query.sql","w+")
    query = \
        """CREATE EXTERNAL TABLE IF NOT EXISTS %s.%s (
        sensor string,temperature_mean float,temperature_perc95 float,
        rms_velocity_z_mean float,rms_velocity_z_perc95 float
        )
        ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe'
        WITH SERDEPROPERTIES (
        'serialization.format' = ',',
        'field.delim' = ',',
        'skip.header.line.count' = '1'
        ) LOCATION '%s'
    TBLPROPERTIES ('has_encrypted_data'='false');""" % (database, table, location)

    f.write(query)
    f.close()
    print("Executing query: %s" % (query))
    run_query(query, database, s3_output)


if __name__== "__main__":
    main(database_name, table_name, s3_output)
