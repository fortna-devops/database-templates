import boto3
import json
import csv
import iso8601
import pytz
import datetime

from boto3.s3.transfer import S3Transfer

table_name_1 = 'Site1'
table_name_2 = 'PROD-Stack-2-RawTable-BNEFGFPUCGWQ'

client = boto3.client('dynamodb',
                      region_name='us-east-1',
                      aws_access_key_id="AKIAJTPB5VVXGZT34HJA",
                      aws_secret_access_key="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",
                      endpoint_url="https://dynamodb.us-east-1.amazonaws.com/")

paginator = client.get_paginator('scan')
operation_parameters = {
    'TableName': table_name_2,
    'PaginationConfig':{'MaxItems': 30230886 , 'PageSize':2000},
   # 'ExclusiveStartKey': {"hash_key": {"S": "2be0b8b01c1fd82e72c0f819573e3a85"}, "read_time": {"S": "2018-05-17T20:07:49.548960-04:00"}}
     }

data =[]
csv_items = []


SENSOR_DATA_FORMAT_MAP = {
    'timestamp' : "read_time",
    'gateway': 'gateway',
    'port': 'port',
    'sensor': 'sensor',
    'rms_velocity_z': 'Z_RMS_IPS',
    'temperature': 'TEMP_F',
    'rms_velocity_x': 'X_RMS_V_IPS',
    'peak_acceleration_z': 'Z_PEAK_G',
    'peak_acceleration_x': 'X_PEAK_G',
    'peak_frequency_z': 'Z_PEAK_V_HZ',
    'peak_frequency_x': 'X_PEAK_V_HZ',
    'rms_acceleration_z': 'Z_RMS_G',
    'rms_acceleration_x': 'X_RMS_G',
    'kurtosis_z': 'Z_KURT',
    'kurtosis_x': 'X_KURT',
    'crest_acceleration_z': 'Z_CREST',
    'crest_acceleration_x': 'X_CREST',
    'peak_velocity_z': 'Z_PEAK_V_IPS',
    'peak_velocity_x': 'X_PEAK_V_IPS',
    'hf_rms_acceleration_z': 'Z_HF_RMS_G',
    'hf_rms_acceleration_x': 'X_HF_RMS_G'
    }



counter = 0
dynamo_sensor_keys = SENSOR_DATA_FORMAT_MAP.values()
athena_sensor_headers = SENSOR_DATA_FORMAT_MAP.keys()


for page in paginator.paginate(**operation_parameters):
    counter +=1
    print('counter=', counter)
    #print('page=', page)

    data =[]
    csv_plc_items = []
    csv_sensor_items = []
    data.extend(page['Items'])
    for itm in page['Items']:
      values = []
      for key in dynamo_sensor_keys:
        if key in itm.keys():
          if key == 'read_time':
            format = '%Y-%m-%dT%H:%M:%S%z'
            date = list(itm[key].values())[0]
            date = iso8601.parse_date(date)
            utc_dt = date.astimezone(pytz.utc)
            utc_dt = utc_dt.strftime("%Y-%m-%d %H:%M:%S")
            values.append(utc_dt)
          else:
            values.append(list(itm[key].values())[0])
        else:
            values.append('')
      csv_sensor_items.append(values)

    if len(csv_sensor_items)>0:
        f = open('./data/sensor_table_'+str(counter)+'.csv', 'w')
        wtr = csv.writer(f, delimiter=',', lineterminator='\n')
        wtr.writerow(athena_sensor_headers)
        for x in csv_sensor_items : wtr.writerow (x)
        f.close()
        client = boto3.client('s3', aws_access_key_id="AKIAJTPB5VVXGZT34HJA",aws_secret_access_key="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")
        transfer = S3Transfer(client)
        transfer.upload_file('./data/sensor_table_'+str(counter)+'.csv', 'mhspredict-site-fedex-louisville', 'sensor/data/sensor_table_'+str(counter)+'.csv')

    f = open('./data/last_evaluated_key.txt', 'w')
    f.write(json.dumps(page['LastEvaluatedKey']))
    f.close()

    f = open('./data/counter.txt', 'w')
    f.write(str(counter))
    f.close()
