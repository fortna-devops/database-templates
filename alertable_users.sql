CREATE EXTERNAL TABLE IF NOT EXISTS `%(bucket_name)s`.`alertable_users` (
  `user_email` STRING,
  `phone` STRING,
  `user_id` STRING,
  `acknowledgements` STRING
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
    'separatorChar' = ',',
    'quoteChar' = '`',
    'skip.header.line.count' = '1'
)

/* Place the table one level above
the (site_name/gateway_name) folder
because this is not dependent on gateway.*/
LOCATION 's3://%(bucket_name)s/alertable_users'
TBLPROPERTIES ('has_encrypted_data'='false');