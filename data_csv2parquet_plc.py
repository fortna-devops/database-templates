import argparse
import logging
import datetime
import time

from dateutil.relativedelta import relativedelta

import s3fs
import pyarrow as pa
import pyarrow.parquet as pq
import pandas as pd
import numpy as np


logger = logging.getLogger()


BUCKET_NAME = 'mhspredict-site-fedex-louisville'
SITE_NAME = 'fedex-louisville'
TABLE_NAME = 'plc'

HUMANIZE_TIME_ATTRS = ['years', 'months', 'days', 'hours', 'minutes', 'seconds']

DATA_PATH = 's3://{}/{}/data/'.format(BUCKET_NAME, TABLE_NAME)


PYARROW_SCHEMA = pa.schema([
    pa.field('timestamp', pa.timestamp('ms')),
    pa.field('gateway', pa.string()),
    pa.field('port', pa.string()),
    pa.field('sensor', pa.string()),
    pa.field('belt_speed', pa.int64()),
    pa.field('vfd_current', pa.float64()),
    pa.field('vfd_frequency', pa.float64()),
    pa.field('vfd_voltage', pa.float64()),
    pa.field('vfd_fault_code', pa.int64()),
    pa.field('sorter_induct_rate', pa.int64()),
    pa.field('sorter_reject_rate', pa.int64()),
    pa.field('status', pa.string())
])


PANDAS_SCHEMA = {
    'gateway': str,
    'port': str,
    'sensor': str,
    'belt_speed': np.float64,
    'vfd_current': np.float64,
    'vfd_frequency': np.float64,
    'vfd_voltage': np.float64,
    'vfd_fault_code': np.float64,
    'sorter_induct_rate': np.float64,
    'sorter_reject_rate': np.float64,
    'status': str
}

NOT_NULL_FILTER_COLUMNS = ['belt_speed', 'vfd_fault_code', 'sorter_induct_rate', 'sorter_reject_rate']


def humanize_timedelta(delta):
    res = []
    for attr in HUMANIZE_TIME_ATTRS:
        value = getattr(delta, attr)
        if not value:
            continue

        if value == 1:
            attr = attr[:-1]

        res.append('{} {}'.format(value, attr))

    return ' '.join(res)


def get_time_delta(t1, t2, humanize=True):
    delta = relativedelta(
        datetime.datetime.fromtimestamp(t1),
        datetime.datetime.fromtimestamp(t2)
    )

    if humanize:
        delta = humanize_timedelta(delta)

    return delta


def main(profile=None):
    start_time = time.time()
    fs = s3fs.S3FileSystem(profile_name=profile)

    parts = []
    files = fs.ls(DATA_PATH)
    len_files = len(files) - 1
    for i, path in enumerate(files):
        logger.info('Loading {}/{}'.format(i, len_files))
        with fs.open(path, 'rb') as f:
            part = pd.read_csv(f, dtype=PANDAS_SCHEMA, parse_dates=[0])
        parts.append(part)
    df = pd.concat(parts)

    # Exclude NaN rows
    df = df.dropna(subset=NOT_NULL_FILTER_COLUMNS)

    # Sort values
    df = df.sort_values(by=['timestamp'])

    # Group by minutes and write by date
    group_keys = (df.timestamp.dt.date, df.timestamp.dt.hour, df.timestamp.dt.minute)
    for (date_key, hour_key, minute_key), subgroup in df.groupby(group_keys):
        logger.info('Writing file for {:%Y-%m-%d} {:02d}:{:02d}'.format(date_key, hour_key, minute_key))
        data = pa.Table.from_pandas(subgroup, schema=PYARROW_SCHEMA)
        date = date_key.strftime('date=%Y-%m-%d')
        path = '/'.join((BUCKET_NAME, SITE_NAME, TABLE_NAME, date))
        pq.write_to_dataset(data, path, filesystem=fs, compression='gzip')

    spend_time = get_time_delta(time.time(), start_time)
    logger.info('Process took {}'.format(spend_time))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--profile', default=None,
                        help='AWS profile name')
    log_group = parser.add_mutually_exclusive_group()
    log_group.add_argument('-d', '--debug', action="store_const",
                           help="print debug info", dest="log_level",
                           const=logging.DEBUG,
                           default=logging.WARNING)
    log_group.add_argument('-v', '--verbose', help="be verbose",
                           action="store_const",
                           dest="log_level", const=logging.INFO)

    args = parser.parse_args()
    args = vars(args)

    log_level = args.pop('log_level')
    logging.basicConfig(
        level=log_level,
        format='[%(levelname)s][%(module)s] %(asctime)s: %(message)s'
    )

    main(**args)
