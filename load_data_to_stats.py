import boto3
import json
import csv
from boto3.s3.transfer import S3Transfer

dynamo_table_1 = 'Sensor_Locations'
dynamo_table_2 = 'PROD-Stack-2-StatsTable-QRLPOUCQJEFX'
database_name_1 = 'mhspredict-site-dhl-miami'
database_name_2 = 'mhspredict-site-fedex-louisville'

client = boto3.client('dynamodb',
                      region_name='us-east-1',
                      aws_access_key_id="AKIAJTPB5VVXGZT34HJA",
                      aws_secret_access_key="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",
                      endpoint_url="https://dynamodb.us-east-1.amazonaws.com/")

paginator = client.get_paginator('scan')
operation_parameters = {
    'TableName': dynamo_table_2,
    'PaginationConfig':{'MaxItems': 100, 'PageSize':1}
}

mapping = {
    'sensor' : 'sensor',
    'equipment': 'equipment',
    'standards_score': 'standards_score',
    'temp_score': 'temp_score',
    'vvrms_score': 'vvrms_score',
    'ac_score': 'ac_score',
    'conveyor': 'conveyor',
    'm_sensor': 'm_sensor',
    'description': 'description',
    'invalid_plc_metrics':'invalid_plc_metrics',
    'location':'location',
    'crest_acceleration_x': 'X_CREST',
    'crest_acceleration_z': 'Z_CREST',
    'hf_rms_acceleration_x': 'X_HF_RMS_G',
    'hf_rms_acceleration_z': 'Z_HF_RMS_G',
    'kurtosis_x': 'X_KURT',
    'kurtosis_z': 'Z_KURT',
    'peak_acceleration_x': 'X_PEAK_G',
    'peak_acceleration_z': 'Z_PEAK_G',
    'peak_frequency_x': 'X_PEAK_V_HZ',
    'peak_frequency_z': 'Z_PEAK_V_HZ',
    'peak_velocity_x': 'X_PEAK_V_IPS',
    'peak_velocity_z': 'Z_PEAK_V_IPS',
    'rms_acceleration_x': 'X_RMS_G',
    'rms_acceleration_z': 'Z_RMS_G',
    'rms_velocity_x': 'X_RMS_IPS',
    'rms_velocity_z': 'Z_RMS_IPS',
    'temperature': 'TEMP_F'

}

mapping_plc ={
    'vfd_voltage' : 'motor_voltage',
    'belt_speed': 'belt_speed',
    'sorter_induct_rate': 'induct_rate',
    'vfd_frequency': 'motor_freq',
    'sorter_reject_rate':'reject_rate',
    'vfd_current':'motor_current',
    'vfd_fault_code': 'vfd_fault_code',
    'status': 'conveyor_status',
    'gateway': 'gateway',
    'plc_score':'plc_score',
    'plc_time' : 'plc_time',
    'port': 'port',
    'timestamp': 'read_time'
}

metrics=['TEMP_F','X_CREST','X_HF_RMS_G','X_KURT','X_PEAK_G','X_PEAK_V_HZ', \
         'X_PEAK_V_IPS','X_RMS_G','X_RMS_V_IPS', \
         'Z_CREST','Z_HF_RMS_G','Z_KURT','Z_PEAK_G','Z_PEAK_V_HZ', \
         'Z_PEAK_V_IPS','Z_RMS_G','Z_RMS_IPS']
plc_metrics=['belt_speed','motor_voltage','induct_rate','motor_freq','reject_rate','motor_current','vfd_fault_code']


additional_fields = {}

for m in metrics:
    for athena_key, dynamo_key in mapping.items():
        if m == dynamo_key:
            additional_fields[athena_key+ '_thr_Y'] = m+ '_thr_Y'
            additional_fields[athena_key + '_thr_R'] = m+ '_thr_R'
            additional_fields[athena_key+'_mean']=m+'_mean'
            additional_fields[athena_key+'_std']=m+'_std'
            additional_fields[athena_key+'_median']=m+'_median'
            additional_fields[athena_key+'_mode']=m+'_mode'
            additional_fields[athena_key+'_min']=m+'_min'
            additional_fields[athena_key+'_max']=m+'_max'
            additional_fields[athena_key+'_perc25']=m+'_perc25'
            additional_fields[athena_key+'_perc75']=m+'_perc75'
            additional_fields[athena_key+'_perc05']=m+'_perc05'
            additional_fields[athena_key+'_perc95']=m+'_perc95'




additional_fields_plc = {}
for m in plc_metrics:
    for athena_key, dynamo_key in mapping_plc.items():
        if m == dynamo_key:
            additional_fields_plc[m+ '_thr_Y'] = m+ '_thr_Y'
            additional_fields_plc[m + '_thr_R'] = m+ '_thr_R'
            additional_fields_plc[m+'_mean']=m+'_mean'
            additional_fields_plc[m+'_std']=m+'_std'
            additional_fields_plc[m+'_median']=m+'_median'
            additional_fields_plc[m+'_mode']=m+'_mode'
            additional_fields_plc[m+'_min']=m+'_min'
            additional_fields_plc[m+'_max']=m+'_max'
            additional_fields_plc[m+'_perc25']=m+'_perc25'
            additional_fields_plc[m+'_perc75']=m+'_perc75'
            additional_fields_plc[m+'_perc05']=m+'_perc05'
            additional_fields_plc[m+'_perc95']=m+'_perc95'

final_keys = {**mapping, **mapping_plc , **additional_fields , **additional_fields_plc}

data =[]
csv_items = []
counter = 0
headers = '"site (S)","sensor (S)","cust (S)","equipment (S)","standards_score (N)","temp_score (N)","vvrms_score (N)","X_RMS_G_thr (N)","ac_score (N)","conveyor (S)","hash_key (S)","m_sensor (S)","tz_offset (S)","description (S)","invalid_plc_metrics (S)","location (S)","TEMP_F_thr_R (N)","TEMP_F_thr_Y (N)","X_CREST_thr_R (N)","X_CREST_thr_Y (N)","X_HF_RMS_G_thr_R (N)","X_HF_RMS_G_thr_Y (N)","X_KURT_thr_R (N)","X_KURT_thr_Y (N)","X_PEAK_G_thr_R (N)","X_PEAK_G_thr_Y (N)","X_PEAK_V_HZ_thr_R (N)","X_PEAK_V_HZ_thr_Y (N)","X_PEAK_V_IPS_thr_R (N)","X_PEAK_V_IPS_thr_Y (N)","X_RMS_G_thr_R (N)","X_RMS_G_thr_Y (N)","X_RMS_V_IPS_thr_R (N)","X_RMS_V_IPS_thr_Y (N)","Z_CREST_thr_R (N)","Z_CREST_thr_Y (N)","Z_HF_RMS_G_thr_R (N)","Z_HF_RMS_G_thr_Y (N)","Z_KURT_thr_R (N)","Z_KURT_thr_Y (N)","Z_PEAK_G_thr_R (N)","Z_PEAK_G_thr_Y (N)","Z_PEAK_V_HZ_thr_R (N)","Z_PEAK_V_HZ_thr_Y (N)","Z_PEAK_V_IPS_thr_R (N)","Z_PEAK_V_IPS_thr_Y (N)","Z_RMS_G_thr_R (N)","Z_RMS_G_thr_Y (N)","Z_RMS_IPS_thr_R (N)","Z_RMS_IPS_thr_Y (N)","belt_speed (N)","conveyor_status (S)","gateway (S)","induct_rate (N)","motor_current (N)","motor_freq (N)","motor_voltage (N)","plc_score (N)","plc_status (S)","plc_time (S)","port (S)","read_time (S)","reject_rate (N)","vfd_fault_code (N)","TEMP_F_max (N)","TEMP_F_mean (N)","TEMP_F_median (N)","TEMP_F_min (N)","TEMP_F_mode (N)","TEMP_F_perc05 (N)","TEMP_F_perc25 (N)","TEMP_F_perc75 (N)","TEMP_F_perc95 (N)","TEMP_F_std (N)","X_CREST_max (N)","X_CREST_mean (N)","X_CREST_median (N)","X_CREST_min (N)","X_CREST_mode (N)","X_CREST_perc05 (N)","X_CREST_perc25 (N)","X_CREST_perc75 (N)","X_CREST_perc95 (N)","X_CREST_std (N)","X_HF_RMS_G_max (N)","X_HF_RMS_G_mean (N)","X_HF_RMS_G_median (N)","X_HF_RMS_G_min (N)","X_HF_RMS_G_mode (N)","X_HF_RMS_G_perc05 (N)","X_HF_RMS_G_perc25 (N)","X_HF_RMS_G_perc75 (N)","X_HF_RMS_G_perc95 (N)","X_HF_RMS_G_std (N)","X_KURT_max (N)","X_KURT_mean (N)","X_KURT_median (N)","X_KURT_min (N)","X_KURT_mode (N)","X_KURT_perc05 (N)","X_KURT_perc25 (N)","X_KURT_perc75 (N)","X_KURT_perc95 (N)","X_KURT_std (N)","X_PEAK_G_max (N)","X_PEAK_G_mean (N)","X_PEAK_G_median (N)","X_PEAK_G_min (N)","X_PEAK_G_mode (N)","X_PEAK_G_perc05 (N)","X_PEAK_G_perc25 (N)","X_PEAK_G_perc75 (N)","X_PEAK_G_perc95 (N)","X_PEAK_G_std (N)","X_PEAK_V_HZ_max (N)","X_PEAK_V_HZ_mean (N)","X_PEAK_V_HZ_median (N)","X_PEAK_V_HZ_min (N)","X_PEAK_V_HZ_mode (N)","X_PEAK_V_HZ_perc05 (N)","X_PEAK_V_HZ_perc25 (N)","X_PEAK_V_HZ_perc75 (N)","X_PEAK_V_HZ_perc95 (N)","X_PEAK_V_HZ_std (N)","X_PEAK_V_IPS_max (N)","X_PEAK_V_IPS_mean (N)","X_PEAK_V_IPS_median (N)","X_PEAK_V_IPS_min (N)","X_PEAK_V_IPS_mode (N)","X_PEAK_V_IPS_perc05 (N)","X_PEAK_V_IPS_perc25 (N)","X_PEAK_V_IPS_perc75 (N)","X_PEAK_V_IPS_perc95 (N)","X_PEAK_V_IPS_std (N)","X_RMS_G_max (N)","X_RMS_G_mean (N)","X_RMS_G_median (N)","X_RMS_G_min (N)","X_RMS_G_mode (N)","X_RMS_G_perc05 (N)","X_RMS_G_perc25 (N)","X_RMS_G_perc75 (N)","X_RMS_G_perc95 (N)","X_RMS_G_std (N)","X_RMS_V_IPS_max (N)","X_RMS_V_IPS_mean (N)","X_RMS_V_IPS_median (N)","X_RMS_V_IPS_min (N)","X_RMS_V_IPS_mode (N)","X_RMS_V_IPS_perc05 (N)","X_RMS_V_IPS_perc25 (N)","X_RMS_V_IPS_perc75 (N)","X_RMS_V_IPS_perc95 (N)","X_RMS_V_IPS_std (N)","Z_CREST_max (N)","Z_CREST_mean (N)","Z_CREST_median (N)","Z_CREST_min (N)","Z_CREST_mode (N)","Z_CREST_perc05 (N)","Z_CREST_perc25 (N)","Z_CREST_perc75 (N)","Z_CREST_perc95 (N)","Z_CREST_std (N)","Z_HF_RMS_G_max (N)","Z_HF_RMS_G_mean (N)","Z_HF_RMS_G_median (N)","Z_HF_RMS_G_min (N)","Z_HF_RMS_G_mode (N)","Z_HF_RMS_G_perc05 (N)","Z_HF_RMS_G_perc25 (N)","Z_HF_RMS_G_perc75 (N)","Z_HF_RMS_G_perc95 (N)","Z_HF_RMS_G_std (N)","Z_KURT_max (N)","Z_KURT_mean (N)","Z_KURT_median (N)","Z_KURT_min (N)","Z_KURT_mode (N)","Z_KURT_perc05 (N)","Z_KURT_perc25 (N)","Z_KURT_perc75 (N)","Z_KURT_perc95 (N)","Z_KURT_std (N)","Z_PEAK_G_max (N)","Z_PEAK_G_mean (N)","Z_PEAK_G_median (N)","Z_PEAK_G_min (N)","Z_PEAK_G_mode (N)","Z_PEAK_G_perc05 (N)","Z_PEAK_G_perc25 (N)","Z_PEAK_G_perc75 (N)","Z_PEAK_G_perc95 (N)","Z_PEAK_G_std (N)","Z_PEAK_V_HZ_max (N)","Z_PEAK_V_HZ_mean (N)","Z_PEAK_V_HZ_median (N)","Z_PEAK_V_HZ_min (N)","Z_PEAK_V_HZ_mode (N)","Z_PEAK_V_HZ_perc05 (N)","Z_PEAK_V_HZ_perc25 (N)","Z_PEAK_V_HZ_perc75 (N)","Z_PEAK_V_HZ_perc95 (N)","Z_PEAK_V_HZ_std (N)","Z_PEAK_V_IPS_max (N)","Z_PEAK_V_IPS_mean (N)","Z_PEAK_V_IPS_median (N)","Z_PEAK_V_IPS_min (N)","Z_PEAK_V_IPS_mode (N)","Z_PEAK_V_IPS_perc05 (N)","Z_PEAK_V_IPS_perc25 (N)","Z_PEAK_V_IPS_perc75 (N)","Z_PEAK_V_IPS_perc95 (N)","Z_PEAK_V_IPS_std (N)","Z_RMS_G_max (N)","Z_RMS_G_mean (N)","Z_RMS_G_median (N)","Z_RMS_G_min (N)","Z_RMS_G_mode (N)","Z_RMS_G_perc05 (N)","Z_RMS_G_perc25 (N)","Z_RMS_G_perc75 (N)","Z_RMS_G_perc95 (N)","Z_RMS_G_std (N)","Z_RMS_IPS_max (N)","Z_RMS_IPS_mean (N)","Z_RMS_IPS_median (N)","Z_RMS_IPS_min (N)","Z_RMS_IPS_mode (N)","Z_RMS_IPS_perc05 (N)","Z_RMS_IPS_perc25 (N)","Z_RMS_IPS_perc75 (N)","Z_RMS_IPS_perc95 (N)","Z_RMS_IPS_std (N)","belt_speed_max (N)","belt_speed_mean (N)","belt_speed_median (N)","belt_speed_min (N)","belt_speed_mode (N)","belt_speed_perc05 (N)","belt_speed_perc25 (N)","belt_speed_perc75 (N)","belt_speed_perc95 (N)","belt_speed_std (N)","belt_speed_thr_R (N)","belt_speed_thr_Y (N)","induct_rate_max (N)","induct_rate_mean (N)","induct_rate_median (N)","induct_rate_min (N)","induct_rate_mode (N)","induct_rate_perc05 (N)","induct_rate_perc25 (N)","induct_rate_perc75 (N)","induct_rate_perc95 (N)","induct_rate_std (N)","induct_rate_thr_R (N)","induct_rate_thr_Y (N)","motor_current_max (N)","motor_current_mean (N)","motor_current_median (N)","motor_current_min (N)","motor_current_mode (N)","motor_current_perc05 (N)","motor_current_perc25 (N)","motor_current_perc75 (N)","motor_current_perc95 (N)","motor_current_std (N)","motor_current_thr_R (N)","motor_current_thr_Y (N)","motor_freq_max (N)","motor_freq_mean (N)","motor_freq_median (N)","motor_freq_min (N)","motor_freq_mode (N)","motor_freq_perc05 (N)","motor_freq_perc25 (N)","motor_freq_perc75 (N)","motor_freq_perc95 (N)","motor_freq_std (N)","motor_freq_thr_R (N)","motor_freq_thr_Y (N)","motor_voltage_max (N)","motor_voltage_mean (N)","motor_voltage_median (N)","motor_voltage_min (N)","motor_voltage_mode (N)","motor_voltage_perc05 (N)","motor_voltage_perc25 (N)","motor_voltage_perc75 (N)","motor_voltage_perc95 (N)","motor_voltage_std (N)","motor_voltage_thr_R (N)","motor_voltage_thr_Y (N)","reject_rate_max (N)","reject_rate_mean (N)","reject_rate_median (N)","reject_rate_min (N)","reject_rate_mode (N)","reject_rate_perc05 (N)","reject_rate_perc25 (N)","reject_rate_perc75 (N)","reject_rate_perc95 (N)","reject_rate_std (N)","reject_rate_thr_R (N)","reject_rate_thr_Y (N)","vfd_fault_code_max (N)","vfd_fault_code_mean (N)","vfd_fault_code_median (N)","vfd_fault_code_min (N)","vfd_fault_code_mode (N)","vfd_fault_code_perc05 (N)","vfd_fault_code_perc25 (N)","vfd_fault_code_perc75 (N)","vfd_fault_code_perc95 (N)","vfd_fault_code_std (N)","vfd_fault_code_thr_R (N)","vfd_fault_code_thr_Y (N)"'
columns_arr = headers.split(",")

keys = final_keys.values()
headers = final_keys.keys()
keys = final_keys.values()
headers = final_keys.keys()
athena_headers = []
dynamo_keys = []
for itm in columns_arr:
    itm = itm.replace("\"", "")
    column = itm.split(" ")[0]
    if column in keys:
        for key, value in final_keys.items():
            if value == column:
                athena_headers.append(key)
                dynamo_keys.append(value)


for page in paginator.paginate(**operation_parameters):
    data =[]
    csv_items = []
    counter +=1
    data.extend(page['Items'])
    for itm in page['Items']:
        values = []
        for key in dynamo_keys:
            if key in itm.keys():
                values.append(list(itm[key].values())[0])
            else:
                values.append(0)
        csv_items.append(values)


        f = open('./stats/stats_table_'+list(itm.get("sensor").values())[0]+'.csv', 'w')
        wtr = csv.writer(f, delimiter=',', lineterminator='\n')
        wtr.writerow(athena_headers)
        for x in csv_items : wtr.writerow (x)
        f.close()

        client = boto3.client('s3', aws_access_key_id="AKIAJTPB5VVXGZT34HJA",aws_secret_access_key="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")
        transfer = S3Transfer(client)
        transfer.upload_file('./stats/stats_table_'+list(itm.get("sensor").values())[0]+'.csv', database_name_2, 'stats/data/stats_table_'+list(itm.get("sensor").values())[0]+'.csv')







