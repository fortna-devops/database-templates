CREATE EXTERNAL TABLE IF NOT EXISTS `mhspredict-site-fedex-louisville`.`pred_output` (
  `timestamp` TIMESTAMP,
  `sensor` STRING,
  `conveyor` STRING,
  `equipment_type` STRING,
  `temperature` DOUBLE,
  `rms_velocity_z` DOUBLE,
  `rms_velocity_x` DOUBLE
)
PARTITIONED BY (`date` DATE)
ROW FORMAT SERDE 'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe'
WITH SERDEPROPERTIES ('serialization.format'='1')
LOCATION 's3://%(bucket_name)s/%(site_name)s/predict/'
TBLPROPERTIES ('has_encrypted_data'='true');
