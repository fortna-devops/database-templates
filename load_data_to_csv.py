from boto3.dynamodb.conditions import Key
import time
import boto3
import csv
import iso8601
import pytz
import json
import os
import hashlib

raw_table_name_1 = 'Site1'
raw_table_name_2 = 'PROD-Stack-2-RawTable-BNEFGFPUCGWQ'
dynamo_table_1 = 'Sensor_Locations'
dynamo_table_2 = 'PROD-Stack-2-LocationTable-1DO3ZXZLD8MFJ'
START_DATE = "2018-07-01T00:00"
END_DATE = "2018-07-31T23:59"
PLC_TRUE_SENSOR_FALSE = False

dynamodb = boto3.resource("dynamodb", region_name='us-east-1')
paginator = dynamodb.meta.client.get_paginator("query")

location_table = dynamodb.Table(dynamo_table_2)
response = location_table.query(
    KeyConditionExpression=Key('site').eq("2")
)
equipment = response['Items']
PLC_DATA_FORMAT_MAP ={
    'timestamp': 'read_time',
    'gateway': 'gateway',
    'port': 'port',
    'sensor': 'sensor',
    'belt_speed': 'belt_speed',
    'vfd_current': 'motor_current',
    'vfd_frequency': 'motor_freq',
    'vfd_voltage': 'motor_voltage',
    'vfd_fault_code': 'vfd_fault_code',
    'sorter_induct_rate': 'induct_rate',
    'sorter_reject_rate': 'reject_rate',
    'status': 'conveyor_status'
}

SENSOR_DATA_FORMAT_MAP = {
    'timestamp': 'read_time',
    'gateway': 'gateway',
    'port': 'port',
    'sensor': 'sensor',
    'rms_velocity_z': 'Z_RMS_IPS',
    'temperature': 'TEMP_F',
    'rms_velocity_x': 'X_RMS_V_IPS',
    'peak_acceleration_z': 'Z_PEAK_G',
    'peak_acceleration_x': 'X_PEAK_G',
    'peak_frequency_z': 'Z_PEAK_V_HZ',
    'peak_frequency_x': 'X_PEAK_V_HZ',
    'rms_acceleration_z': 'Z_RMS_G',
    'rms_acceleration_x': 'X_RMS_G',
    'kurtosis_z': 'Z_KURT',
    'kurtosis_x': 'X_KURT',
    'crest_acceleration_z': 'Z_CREST',
    'crest_acceleration_x': 'X_CREST',
    'peak_velocity_z': 'Z_PEAK_V_IPS',
    'peak_velocity_x': 'X_PEAK_V_IPS',
    'hf_rms_acceleration_z': 'Z_HF_RMS_G',
    'hf_rms_acceleration_x': 'X_HF_RMS_G'
}

if PLC_TRUE_SENSOR_FALSE:
    dynamo_plc_keys = PLC_DATA_FORMAT_MAP.values()
    athena_plc_headers = PLC_DATA_FORMAT_MAP.keys()
else:
    dynamo_plc_keys = SENSOR_DATA_FORMAT_MAP.values()
    athena_plc_headers = SENSOR_DATA_FORMAT_MAP.keys()

try:
    os.makedirs('./data/plc')
except os.error:
    pass
try:
    os.makedirs('./data/sensor')
except os.error:
    pass

pages_counter = 0
for equip in equipment:
    md5 = hashlib.md5()
    if PLC_TRUE_SENSOR_FALSE:
        md5.update(('FedEx_ELOU400_enip_' + equip.get('sensor')).encode("utf-8"))
    else:
        md5.update(('FedEx_ELOU400_4_rs485_' + equip.get('sensor')).encode("utf-8"))
    equip['hash_key'] = md5.hexdigest()
    print(equip.get('sensor'), equip['hash_key'])
    if ((PLC_TRUE_SENSOR_FALSE and "PLC" in equip.get('sensor'))
        or (not PLC_TRUE_SENSOR_FALSE and "PLC" not in equip.get('sensor')))\
            and 'ALL_' not in equip.get('sensor'):
        pages = paginator.paginate(TableName=raw_table_name_2,
                                   KeyConditionExpression=Key('hash_key').eq(equip.get('hash_key')) & Key('read_time').between(START_DATE, END_DATE),
                                   PaginationConfig={"PageSize": 2000}#,
                                   #ExclusiveStartKey={"hash_key": "ddfcaed74a83d3b36c8ef6060d447387", "read_time": "2018-06-30T22:48:55.071252-04:00"}
                                   )
        for page in pages:
            csv_items = []
            plc = page['Items']
            for data in plc:
                values = []
                for dynamo_key in dynamo_plc_keys:
                    if dynamo_key in data.keys():
                        if dynamo_key == 'read_time':
                            date = data[dynamo_key]
                            date = iso8601.parse_date(date)
                            utc_dt = date.astimezone(pytz.utc)
                            utc_dt = utc_dt.strftime("%Y-%m-%d %H:%M:%S")
                            values.append(utc_dt)
                        else:
                            values.append(data[dynamo_key])
                    else:
                        values.append('')
                csv_items.append(values)
            pages_counter += 1
            print('pages_counter',pages_counter, equip.get('sensor'), len(csv_items))
            if len(csv_items) > 0:
                if PLC_TRUE_SENSOR_FALSE:
                    f = open('./data/plc/table_'+str(pages_counter)+'.csv',
                             'w')
                else:
                    f = open('./data/sensor/table_' + str(pages_counter) \
                             + '.csv', 'w')
                wtr = csv.writer(f, delimiter=',', lineterminator='\n')
                wtr.writerow(athena_plc_headers)
                for x in csv_items:
                    wtr.writerow (x)
                f.close()
            else:
                print("WARNING len(csv_items) <= 0")

            if 'LastEvaluatedKey' in page:
                f = open('./data/last_evaluated_key.txt', 'w')
                f.write(json.dumps(page['LastEvaluatedKey']))
                f.close()

            time.sleep(2)
