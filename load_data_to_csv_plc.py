from datetime import datetime
from datetime import timedelta
import math
from boto3.dynamodb.conditions import Key, Attr
import logging
import json
import time
import os
from datetime import timezone
from decimal import Decimal
import boto3
from boto3.s3.transfer import S3Transfer
import csv
import iso8601
import pytz


raw_table_name_1 = 'Site1'
raw_table_name_2 = 'PROD-Stack-2-RawTable-BNEFGFPUCGWQ'
dynamo_table_1 = 'Sensor_Locations'
dynamo_table_2 = 'PROD-Stack-2-LocationTable-1DO3ZXZLD8MFJ'
TZ_OFFSET = -4
FILTER_INTERVAL = 365

dynamodb = boto3.resource("dynamodb", region_name='us-east-1')
paginator = dynamodb.meta.client.get_paginator("query")

location_table = dynamodb.Table(dynamo_table_2)
response = location_table.query(
    KeyConditionExpression=Key('site').eq("2")
)
equipment=response['Items']
PLC_DATA_FORMAT_MAP ={
    'timestamp' : "read_time",
    'gateway': 'gateway',
    'port': 'port',
    'sensor': 'sensor',
    'belt_speed': 'belt_speed',
    'vfd_current':'motor_current',
    'vfd_frequency': 'motor_freq',
    'vfd_voltage' : 'motor_voltage',
    'vfd_fault_code': 'vfd_fault_code',
    'sorter_induct_rate': 'induct_rate',
    'sorter_reject_rate':'reject_rate',
    'status': 'conveyor_status'
}
dynamo_plc_keys = PLC_DATA_FORMAT_MAP.values()
athena_plc_headers = PLC_DATA_FORMAT_MAP.keys()
pages_counter = 0
for equip in equipment:


  if "PLC" in equip.get('sensor') and equip.get('sensor') != "ALL_PLC":
    nw=datetime.now(timezone(timedelta(hours=TZ_OFFSET))).strftime("%Y-%m-%dT%H:%M")
    before=(datetime.now(timezone(timedelta(hours=TZ_OFFSET))) - timedelta(days=FILTER_INTERVAL)).strftime("%Y-%m-%dT%H:%M")

    pages = paginator.paginate(TableName=raw_table_name_2,
                           KeyConditionExpression=Key('hash_key').eq(equip.get('hash_key')) & Key('read_time').between(before,nw),
                           PaginationConfig={"PageSize": 2000},
    # 'ExclusiveStartKey': {"hash_key": {"S": "2be0b8b01c1fd82e72c0f819573e3a85"}, "read_time": {"S": "2018-05-17T20:07:49.548960-04:00"}}
     )

    for page in pages:
        csv_plc_items = []
        plc = page['Items']
        for plc_data in plc:
            values=[]
            for dynamo_key in dynamo_plc_keys:
                if dynamo_key in plc_data.keys():
                    if dynamo_key == 'read_time':
                        format = '%Y-%m-%dT%H:%M:%S%z'
                        date = plc_data[dynamo_key]
                        date = iso8601.parse_date(date)
                        utc_dt = date.astimezone(pytz.utc)
                        utc_dt = utc_dt.strftime("%Y-%m-%d %H:%M:%S")
                        values.append(utc_dt)
                    else:
                        values.append(plc_data[dynamo_key])
                else:
                    values.append('')
            csv_plc_items.append(values)
        pages_counter += 1
        print('pages_counter',pages_counter, equip.get('sensor') )
        if len(csv_plc_items)>0:
            f = open('./plc/plc_table_'+str(pages_counter)+'.csv', 'w')
        wtr = csv.writer(f, delimiter=',', lineterminator='\n')
        wtr.writerow(athena_plc_headers)
        for x in csv_plc_items : wtr.writerow (x)
        f.close()

        client = boto3.client('s3', aws_access_key_id="AKIAJTPB5VVXGZT34HJA",aws_secret_access_key="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")
        transfer = S3Transfer(client)
        transfer.upload_file('./plc/plc_table_'+str(pages_counter)+'.csv', 'mhspredict-site-fedex-louisville', 'plc/data/plc_table_'+str(pages_counter)+'.csv')
        time.sleep(2)


