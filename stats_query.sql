CREATE EXTERNAL TABLE IF NOT EXISTS `%(bucket_name)s`.`stats` (
  `sensor` STRING,
  `temperature_mean` FLOAT,
  `temperature_perc95` FLOAT,
  `rms_velocity_z_mean` FLOAT,
  `rms_velocity_z_perc95` FLOAT,
  `hf_rms_acceleration_x_mean` FLOAT,
  `hf_rms_acceleration_x_perc95` FLOAT,
  `hf_rms_acceleration_z_mean` FLOAT,
  `hf_rms_acceleration_z_perc95` FLOAT,
  `rms_velocity_x_mean` FLOAT,
  `rms_velocity_x_perc95` FLOAT
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe'
WITH SERDEPROPERTIES (
  'serialization.format' = ',',
  'field.delim' = ',',
  'skip.header.line.count' = '1'
)
LOCATION 's3://%(bucket_name)s/%(site_name)s/stats'
TBLPROPERTIES ('has_encrypted_data'='false');