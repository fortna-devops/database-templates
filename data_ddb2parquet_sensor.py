import argparse
import logging
import os
from datetime import datetime, timezone, timedelta, time as dt_time
from collections import OrderedDict
from io import BytesIO

import boto3
import s3fs
from boto3.dynamodb.conditions import Key

import pandas as pd
import pyarrow as pa
import pyarrow.parquet as pq
import numpy as np

logger = logging.getLogger()


# DHL
SITE_NAME = 'dhl-miami'
BUCKET_NAME = 'mhspredict-site-dhl-miami'
RAW_TABLE = 'Site1'
LOCATION_TABLE = 'Sensor_Locations'
SITE_ID = '1'
SENSOR_FILTER = "WHERE sensor != '9' AND sensor != '79'"

# FEDEX
# SITE_NAME = 'fedex-louisville'
# BUCKET_NAME = 'mhspredict-site-fedex-louisville'
# RAW_TABLE = 'PROD-Stack-2-RawTable-BNEFGFPUCGWQ'
# LOCATION_TABLE = 'PROD-Stack-2-LocationTable-1DO3ZXZLD8MFJ'
# SITE_ID = '2'
# SENSOR_FILTER = ''


TABLE_NAME = 'sensor'
ATHENA_TABLE = '"{}"."{}"'.format(BUCKET_NAME, TABLE_NAME)
RESULT_PATH = 's3://{}/tmp/'.format(BUCKET_NAME)

TZ_OFFSET = -4
PAGE_SIZE = 2000


DDB_KEYS = [
    'read_time',
    'gateway',
    'port',
    'sensor',
    'Z_RMS_IPS',
    'TEMP_F',
    'X_RMS_V_IPS',
    'Z_PEAK_G',
    'X_PEAK_G',
    'Z_PEAK_V_HZ',
    'X_PEAK_V_HZ',
    'Z_RMS_G',
    'X_RMS_G',
    'Z_KURT',
    'X_KURT',
    'Z_CREST',
    'X_CREST',
    'Z_PEAK_V_IPS',
    'X_PEAK_V_IPS',
    'Z_HF_RMS_G',
    'X_HF_RMS_G'
]

ATHENA_KEYS = [
    'timestamp',
    'gateway',
    'port',
    'sensor',
    'rms_velocity_z',
    'temperature',
    'rms_velocity_x',
    'peak_acceleration_z',
    'peak_acceleration_x',
    'peak_frequency_z',
    'peak_frequency_x',
    'rms_acceleration_z',
    'rms_acceleration_x',
    'kurtosis_z',
    'kurtosis_x',
    'crest_acceleration_z',
    'crest_acceleration_x',
    'peak_velocity_z',
    'peak_velocity_x',
    'hf_rms_acceleration_z',
    'hf_rms_acceleration_x'
]

MAPPING = OrderedDict((DDB_KEYS[i], ATHENA_KEYS[i]) for i in range(len(DDB_KEYS)))


DDB_SCHEMA = OrderedDict((
    ('read_time', str),
    ('gateway', str),
    ('port', str),
    ('sensor', str),
    ('Z_RMS_IPS', np.float64),
    ('TEMP_F', np.float64),
    ('X_RMS_V_IPS', np.float64),
    ('Z_PEAK_G', np.float64),
    ('X_PEAK_G', np.float64),
    ('Z_PEAK_V_HZ', np.float64),
    ('X_PEAK_V_HZ', np.float64),
    ('Z_RMS_G', np.float64),
    ('X_RMS_G', np.float64),
    ('Z_KURT', np.float64),
    ('X_KURT', np.float64),
    ('Z_CREST', np.float64),
    ('X_CREST', np.float64),
    ('Z_PEAK_V_IPS', np.float64),
    ('X_PEAK_V_IPS', np.float64),
    ('Z_HF_RMS_G', np.float64),
    ('X_HF_RMS_G', np.float64)
))

PYARROW_SCHEMA = pa.schema([
    pa.field('timestamp', pa.timestamp('ms')),
    pa.field('gateway', pa.string()),
    pa.field('port', pa.string()),
    pa.field('sensor', pa.string()),
    pa.field('rms_velocity_z', pa.float64()),
    pa.field('temperature', pa.float64()),
    pa.field('rms_velocity_x', pa.float64()),
    pa.field('peak_acceleration_z', pa.float64()),
    pa.field('peak_acceleration_x', pa.float64()),
    pa.field('peak_frequency_z', pa.float64()),
    pa.field('peak_frequency_x', pa.float64()),
    pa.field('rms_acceleration_z', pa.float64()),
    pa.field('rms_acceleration_x', pa.float64()),
    pa.field('kurtosis_z', pa.float64()),
    pa.field('kurtosis_x', pa.float64()),
    pa.field('crest_acceleration_z', pa.float64()),
    pa.field('crest_acceleration_x', pa.float64()),
    pa.field('peak_velocity_z', pa.float64()),
    pa.field('peak_velocity_x', pa.float64()),
    pa.field('hf_rms_acceleration_z', pa.float64()),
    pa.field('hf_rms_acceleration_x', pa.float64()),
])


def get_result(client, fs, qid):
    while True:  # polling result
        state = client.get_query_execution(
            QueryExecutionId=qid
        )['QueryExecution']['Status']['State']

        if state == 'SUCCEEDED':
            tmp_file = RESULT_PATH + qid + '.csv'

            data = BytesIO()
            with fs.open(tmp_file, 'rb') as fl:
                data.write(fl.read())
            data.seek(0)

            fs.rm(tmp_file)
            fs.rm(tmp_file + '.metadata')
            return data
        if state == 'FAILED':
            raise BaseException('Request to Athena has FAILED')


def get_latest(client, fs):
    query_str = """
    SELECT max(timestamp) as max_timestamp 
    FROM {}
    {}
    GROUP BY sensor
    """.format(ATHENA_TABLE, SENSOR_FILTER)

    query = client.start_query_execution(
        QueryString=query_str,
        ResultConfiguration={
            'OutputLocation': RESULT_PATH
        }
    )

    result = get_result(client, fs, query['QueryExecutionId'])
    latest = pd.read_csv(result, parse_dates=[0]).min()[0].to_pydatetime()
    return latest


def daterange(start_date, end_date):
    for n in range(int((end_date - start_date).days)):
        d = start_date + timedelta(n)
        yield datetime.combine(d, dt_time()).replace(tzinfo=timezone.utc), \
              datetime.combine(d, dt_time(23, 59, 59)).replace(tzinfo=timezone.utc)


def main(profile=None):
    session = boto3.Session(profile_name=profile)
    ddb = session.resource('dynamodb')
    athena = session.client('athena')
    fs = s3fs.S3FileSystem(profile_name=profile)
    paginator = ddb.meta.client.get_paginator('query')

    location_table = ddb.Table(LOCATION_TABLE)
    response = location_table.query(
        KeyConditionExpression=Key('site').eq(SITE_ID)
    )
    equipment = response['Items']

    logger.info('Getting latest date')
    # latest_date = get_latest(athena, fs)
    latest_date = datetime(2018, 9, 5)
    now = datetime.now()
    tz_offset = timezone(timedelta(hours=TZ_OFFSET))

    for start_date, end_date in daterange(latest_date, now):
        logger.info('Getting data from {:%Y-%m-%d %H:%M} to {:%Y-%m-%d %H:%M}'.format(start_date, end_date))
        result = []
        for equip in equipment:
            if not equip.get('sensor').isdigit():
                continue

            pages = paginator.paginate(
                TableName=RAW_TABLE,
                KeyConditionExpression=Key('hash_key').eq(equip.get('hash_key')) &
                                       Key('read_time').between(
                                           start_date.astimezone(tz_offset).strftime("%Y-%m-%dT%H:%M%z"),
                                           end_date.astimezone(tz_offset).strftime("%Y-%m-%dT%H:%M%z")
                                       ),
                PaginationConfig={'PageSize': PAGE_SIZE}
            )

            for page in pages:
                items = page['Items']

                sensor_df = pd.DataFrame.from_records(items, columns=DDB_KEYS)
                sensor_df = sensor_df.astype(DDB_SCHEMA)
                sensor_df = sensor_df.rename(columns=MAPPING)
                sensor_df['timestamp'] = pd.to_datetime(sensor_df['timestamp']).astype('datetime64[ms]')
                result.append(sensor_df)

        df = pd.concat(result)
        df = df.sort_values(by=['timestamp'])

        print(df.info())

        for key, subgroup in df.groupby(pd.Grouper(key='timestamp', freq='min')):
            logger.info('Writing {} file for {:%Y-%m-%d %H:%M}'.format(TABLE_NAME, key))
            data = pa.Table.from_pandas(subgroup, schema=PYARROW_SCHEMA)
            date = key.strftime('date=%Y-%m-%d')
            path = os.path.join('data', SITE_NAME, TABLE_NAME, date)
            # path = '/'.join((BUCKET_NAME, SITE_NAME, table_name, date))
            # pq.write_to_dataset(data, path, filesystem=fs, compression='gzip')
            pq.write_to_dataset(data, path, compression='gzip')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--profile', default=None,
                        help='AWS profile name')
    log_group = parser.add_mutually_exclusive_group()
    log_group.add_argument('-d', '--debug', action="store_const",
                           help="print debug info", dest="log_level",
                           const=logging.DEBUG,
                           default=logging.WARNING)
    log_group.add_argument('-v', '--verbose', help="be verbose",
                           action="store_const",
                           dest="log_level", const=logging.INFO)

    args = parser.parse_args()
    args = vars(args)

    log_level = args.pop('log_level')
    logging.basicConfig(
        level=log_level,
        format='[%(levelname)s][%(module)s] %(asctime)s: %(message)s'
    )

    main(**args)