"""
Module for migrating 'alarms' data from Athena to RDS.

..author: Tosin Sonuyi <oluwatosinsonuyi@mhsinc.net>
"""

import argparse
import json

import boto3
from datetime import datetime
import logging
from typing import List

# import services
from mhs_rdb import Connector, Alarms, Sensors
from mhs_rdb.models import AlarmsModel, AlarmsTypeEnum, AlarmsMetricEnum, LevelsEnum

from migration_utils import get_dates_list, get_then_insert_data

logger = logging.getLogger()

LOCAL_LOG_FORMAT = '[%(levelname)s][%(module)s] %(asctime)s: %(message)s'
BUCKET_PREFIX = 'mhspredict-site'

RDS_HOST = 'mhspredict-dev.cluster-c2z9g3wxqyud.us-east-1.rds.amazonaws.com'
RDS_USERNAME = 'developer'


def insert_alarms_data(site_name: str, table_name: str, data: List[dict], db_session, boto_session: boto3.Session) -> None:
    """
    Insert alarms data into RDS.

    :param site_name: the name of the site
    :type site_name: str

    :param boto_session: boto3 session
    :type boto_session: boto3 Session

    :returns: None
    :rtype: None
    """

    g_name = site_name.replace('_', '-')

    logger.info(f'attempting to insert {table_name} data into RDS...')

    sensors_s = Sensors(db_session)
    alarms_s = Alarms(db_session)

    # Map sensor_hour row to sensor ID from sensors table.
    sensor_id_map = {}
    unique_sensors = set()
    conveyor_map = dict()
    sensors = sensors_s.get_all()
    for sensor in sensors:
        unique_id = (sensor.name, sensor.gateway_name)
        sensor_id_map[unique_id] = sensor.id

        # Special case for ups
        unique_id = (sensor.name, sensor.gateway_name.split(':')[0])
        sensor_id_map[unique_id] = sensor.id

        conveyor_map[sensor.conveyor.name] = sensor.id
        unique_sensors.add(sensor.name)


    # mapping enums to values
    alarm_type_enum_map = {
        'threshold': AlarmsTypeEnum.ISO_SPIKE,
        'ISO_PERSISTENT': AlarmsTypeEnum.ISO_PERSISTENT,
        'MTD_Alarms': AlarmsTypeEnum.MTD,
        'ml_alarms': AlarmsTypeEnum.ML_SPIKE
    }

    # note, using same metric enums for ml_alarms and iso alarms.
    alarm_metric_enum_map = {
        'rms_velocity_x': AlarmsMetricEnum.rms_velocity_x,
        'rms_velocity_z': AlarmsMetricEnum.rms_velocity_z,
        'temperature': AlarmsMetricEnum.temperature,
        'rms_velocity_x_residuals': AlarmsMetricEnum.rms_velocity_x,
        'rms_velocity_z_residuals': AlarmsMetricEnum.rms_velocity_z,
        'temperature_residuals': AlarmsMetricEnum.temperature,
        'transition_number': AlarmsMetricEnum.transition_number,
    }

    alarm_type_level_map = {
        AlarmsTypeEnum.ISO_SPIKE: LevelsEnum.equipment,
        AlarmsTypeEnum.ISO_PERSISTENT: LevelsEnum.equipment,
        AlarmsTypeEnum.ML_SPIKE: LevelsEnum.equipment,
        AlarmsTypeEnum.MTD: LevelsEnum.conveyor,
    }

    models_list = []
    for data_row in data:

        if data_row['sensor'] == 'NULL' and data_row['equipment'] == 'CONVEYOR':
            s_id = conveyor_map[data_row['conveyor']]

        elif data_row['sensor'] in unique_sensors:
            s_id = sensor_id_map[(data_row['sensor'], g_name)]

        else:
            logger.warning(f'encountered sensor: {data_row["sensor"]} not in sensors table')
            continue

        try:

            # Create predicted data model.
            # to support old alarms w/o data_timestamp column
            if data_row['data_timestamp']:
                timestamp2use4data_timestamp = data_row['data_timestamp']
            else:
                logger.debug('Alarm encountered w/ no data_timestamp column, using timestamp column')
                timestamp2use4data_timestamp = data_row['timestamp']
            try:
                enum_metric = alarm_metric_enum_map[data_row['key']]
            except KeyError:
                logger.warning(f'alarm data has key/metric {data_row["key"]} which is not part of AlarmsMetricEnum enumeration')
                continue

            try:
                enum_type = alarm_type_enum_map[data_row['type']]
            except KeyError:
                logger.warning(f'alarm data has type {data_row["type"]} which is not part of AlarmsTypeEnum enumeration')
                continue

            alarm_level = alarm_type_level_map[enum_type]

            # We don't need JSON like criteria. float is enough
            severity = int(data_row['severity'])
            value = float(data_row['value'])
            criteria = json.loads(data_row['criteria'])

            if enum_type == AlarmsTypeEnum.ISO_SPIKE:
                if value >= criteria['threshold']['red']:
                    criteria = criteria['threshold']['red']
                elif value >= criteria['threshold']['orange']:
                    criteria = criteria['threshold']['orange']
                else:
                    logger.warning('Unknown criteria value')
                    continue

            elif enum_type == AlarmsTypeEnum.ISO_PERSISTENT:
                pass

            elif enum_type == AlarmsTypeEnum.MTD:
                criteria = criteria['transition_threshold']
                enum_metric = AlarmsMetricEnum.transition_number

            elif enum_type == AlarmsTypeEnum.ML_SPIKE:
                low_red, low_orange, high_orange, high_red = criteria[data_row['key']]
                if value <= low_red:
                    criteria = low_red
                elif value <= low_orange:
                    criteria = low_orange
                elif value >= high_orange:
                    criteria = high_orange
                elif value >= high_red:
                    criteria = high_red
                else:
                    logger.warning('Unknown criteria value')
                    continue

            else:
                logger.warning(f'Unknown alarm type "{enum_type}"')
                continue

            model = AlarmsModel(
                sensor_id=s_id,
                created_at=data_row['timestamp'],
                severity=severity,
                type=enum_type,
                metric=enum_metric,
                value=value,
                criteria=float(criteria),
                level=alarm_level,
                # current interpretation of description column.. for now include key and value
                # just needs to be something human readable 
                description=f"{data_row['key']} - {data_row['value']}",
                data_timestamp=timestamp2use4data_timestamp
                )

            models_list.append(model)
        except ValueError:
            logger.warning(f'{table_name} Data unable to be converted into ORM Model: {data_row}\n')
            continue

    alarms_s.insert_all(models_list)

    if models_list:

        logger.debug(f"...sucessfully inserted {len(models_list)} rows of data into '{table_name}' table in RDS")
        insert_success_rate = float(len(models_list))/float(len(data))
        logger.info(f"\nSuccessfully migrated {insert_success_rate*100.}% of data\n")
    else:
        logger.info(f"No models created from the {len(data)} data rows retrieved")


def migrate(site_name, athena_table_name, start_date=datetime.utcnow(), days_back=20, profile=None, rds_host=RDS_HOST, rds_user=RDS_USERNAME):
    """
    migrate data by looping through list of dates
    """

    boto_session = boto3.Session(profile_name=profile)
    dates_list = get_dates_list(start_date=start_date, days_back=days_back)
    rds = Connector(boto_session, host=rds_host, username=rds_user)
    schema = site_name.replace('-', '_')
    database, _ = schema.split('_', 1)
    db_session = rds.get_session(database, schema)
    bucket_name = f'{BUCKET_PREFIX}-{site_name}'
    for date in dates_list:
        full_path = f"s3://{bucket_name}/{athena_table_name}/date={date}"
        get_then_insert_data(bucket_prefix=BUCKET_PREFIX,
                             site_name=site_name,
                             date2query=date,
                             days_back=days_back,
                             boto_session=boto_session,
                             insert_func=insert_alarms_data,
                             db_session=db_session,
                             athena_table_name=athena_table_name,
                             use_athena=True,
                             filepath=full_path)


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--profile', help='AWS profile name')

    log_group = parser.add_mutually_exclusive_group()
    log_group.add_argument('-d', '--debug', action="store_const",
                           help="print debug info", dest="log_level", const=logging.DEBUG,
                           default=logging.INFO)
    log_group.add_argument('-s', '--silent', help="be silent", action="store_const",
                           dest="log_level", const=logging.CRITICAL)

    parser.add_argument('site_name', type=str, help='site to migrate')
    parser.add_argument('days_back', type=int, help='days of data to migrate')

    args = parser.parse_args()
    args = vars(args)

    args['athena_table_name'] = 'alarms'
    logging.basicConfig(level=args.pop('log_level'), format=LOCAL_LOG_FORMAT)

    logger.info(f"processing {args['site_name']} {args['athena_table_name']} data for migration to RDS")
    migrate(**args)
