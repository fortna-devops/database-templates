#!/usr/bin/env python3
"""
..author: Coco Wu <jiewu@mhsinc.net>
..author: Tosin Sonuyi <oluwatosinsonuyi@mhsinc.net>
"""

import logging
import s3fs
import boto3
import argparse
import time
import os
from datetime import datetime

import pandas as pd
import pyarrow.parquet as pq
from typing import List

from migration_utils import get_dates_list, chunks

from mhs_rdb import Connector, PLCDataMinute, PLCDataHour, Sensors
from analytics_sdk_core.data_init.data_pull import DataQueryEngineLite as DQE
from analytics_sdk_core import constants

logger = logging.getLogger()
logger.setLevel(logging.INFO)

BUCKET_PREFIX = 'mhspredict-site'
TABLE_NAME = 'plc'
# START_FROM = 'date=2019-04-05'
# DATA_PATH = 's3://{}/{}/{}'.format(BUCKET_NAME, SITE_NAME, TABLE_NAME)


LOCAL_LOG_FORMAT = '[%(levelname)s][%(module)s] %(asctime)s: %(message)s'

RDS_HOST = 'mhspredict-dev.cluster-c2z9g3wxqyud.us-east-1.rds.amazonaws.com'
RDS_USERNAME = 'developer'


PLC_SCHEMA = {
    'sensor_id': str,
    'timestamp': int,
    'port': str,
    'belt_speed': int,
    'vfd_current': float,
    'vfd_frequency': float,
    'vfd_fault_code': float,
    'sorter_induct_rate': int,
    'sorter_reject_rate': int,
    'status': str,
    'torque': int,
    'hours_running': int,
    'power': int,
    'rpm': int,
    'horsepower_nameplate': int,
    'voltage_nameplate': int,
    'run_speed_nameplate': int,
    'current_nameplate': float,
    'num_phases_nameplate': int
}

def format_query_results_as_df(result_lines):
    """
    Take result lines from athena query and
    turn them into a pandas dataframe with numeric
    column datatypes where applicable
    """

    if len(result_lines) > 0:
        # make a dataframe
        df = pd.DataFrame(result_lines)

        # convert to numeric datatypes where applicable, otherwise ignore
        df = df.apply(pd.to_numeric, errors='ignore')
        return df

    else:
        logger.warning('Query returned an empty result')
        return pd.DataFrame()

def set_datetime_index(df, drop_datetimes=True):
    """
    Create datetime as index
    """
    dt_col = set(df.columns) & set(constants.TIMESTAMP_COLS)
    if len(dt_col) < 1:
        logger.info('No datetime column found in {}, hopefully already an index'.format(df.columns))
        return df

    elif len(dt_col) > 1:
        dt_col = set([dt_col.pop()])
        logger.warning('found multiple datetime columns, using: {}'.format(dt_col))
    try:
        dt_col = dt_col.pop()
        df.index = pd.DatetimeIndex(df[dt_col])
    # TODO: should we raise this error?
    except ValueError as ve:
        logger.error('incompatible datetime string format: {}'.format(ve))
        return df

    if drop_datetimes:
        for tstamp in constants.TIMESTAMP_COLS:
            if tstamp in df.columns:
                df = df.drop(columns=[tstamp])
    return df


def get_avg_and_insert_plc_data(
                         site_name: str, 
                         date2query,
                         boto_session: boto3.Session,
                         insert_func,
                         db_session,
                         athena_table_name,
                         rds_table_name,
                         use_athena=True,
                         filepath=None,
                         fillna_value=None) -> List[dict]:

    """
    Get data and then call a PLC insert function 
    Taken from migration_utils and adapted for specific PLC use case

    :param insert_func: implemented function for specific insertion logic
    :type insert_func: function

    :returns: list of data retrieved from S3
    :rtype: list of dict
    """

    bucket_name = f'{BUCKET_PREFIX}-{site_name}'

    fs = s3fs.S3FileSystem(session=boto_session)

    dqe = DQE(bucket_name)

    if use_athena:
        query = f"""SELECT * from {athena_table_name} WHERE date = date '{date2query}';"""
        data = dqe.execute_generic_query(query)
        df = format_query_results_as_df(data)
        df = set_datetime_index(df, drop_datetimes=False)

    else:
        logger.info(f'attempting to get {athena_table_name} data from S3 for {date2query}...')

        # https://arrow.apache.org/docs/python/generated/pyarrow.parquet.ParquetDataset.html
        # df = pq.ParquetDataset(filepath, filesystem=fs).read_pandas().to_pandas()
        # data = df.to_dict('index')

        dataset = pq.ParquetDataset(filepath, filesystem=fs, validate_schema=False)
        data = dataset.read()
        df = data.to_pandas()
        df.index = df['timestamp']


    # added the following lines just to make it working with mhsdataart.
    # df = df[df['sensor'] == 'PLC-T2-F2-1']
    # df['gateway'] = 'mhs-dataart'
    for column in PLC_SCHEMA:
        if column not in df.columns:
            df[column] = pd.Series()

    
    # import pdb
    # pdb.set_trace()

    if 'plc_data_hour' == rds_table_name:

        # hour downsample
        avg_data_hour = df.groupby([df.gateway, df.port, df.sensor]).resample('H').mean().reset_index()
        avg_data_hour['belt_speed'] = avg_data_hour['belt_speed'].fillna(fillna_value).astype(int)
        avg_data_hour['vfd_fault_code'] = avg_data_hour['vfd_fault_code'].fillna(fillna_value).astype(int)
        avg_data_hour['sorter_induct_rate'] = avg_data_hour['sorter_induct_rate'].fillna(fillna_value).astype(int)
        avg_data_hour['sorter_reject_rate'] = avg_data_hour['sorter_reject_rate'].fillna(fillna_value).astype(int)
        avg_data_hour['status'] = pd.Series()

        avg_data_rows = list(avg_data_hour.to_dict('index').values())

    elif 'plc_data_minute' == rds_table_name:

        # no downsampling necessary for PLC minute
        avg_data_rows = list(df.to_dict('index').values())

    else:
        raise ValueError(f'Unknown table name "{rds_table_name}"')

    logger.info(f'...successfully read {len(data)} rows of {athena_table_name} data from data source')

    insert_func(site_name=site_name,
                rds_table_name=rds_table_name,
                plc_data=avg_data_rows,
                db_session=db_session,
                boto_session=boto_session)


def insert_plc_data(site_name, rds_table_name: str, plc_data: List[dict], db_session, boto_session: boto3.Session) -> None:
    """
    Insert plc data into RDS.

    :param table_name: specifies 'plc_data_hour' or 'plc_data_minute' table
    :type table_name: str

    :param plc_data: plc data returned from S3
    :type plc_data: list of dict

    :param boto_session: boto3 session
    :type boto_session: boto3 Session

    :returns: None
    :rtype: None
    """

    assert rds_table_name in ['plc_data_hour',
                          'plc_data_minute'], "'plc_data_hour' and 'plc_data_minute' are only valid table names"

    logger.info(f'attempting to insert {rds_table_name} data into RDS...')

    sensors_s = Sensors(db_session)
    plc_data_s = PLCDataHour(db_session) if rds_table_name == 'plc_data_hour' else PLCDataMinute(db_session)

    # Map sensor_hour row to sensor ID from sensors table.
    sensor_id = {}
    sensors = sensors_s._get_all()
    for sensor in sensors:
        unique_id = (sensor.name, sensor.gateway_name)
        sensor_id[unique_id] = sensor.id

    total_inserted = 0
    for chunk in chunks(plc_data):
        models_list = []
        for plc_data_row in chunk:
            # Format timestamp to correct type expected by SQLAlchemy and PostgreSQL.
            # if not isinstance(plc_data_row['timestamp'], int):
            #     if isinstance(plc_data_row['timestamp'], str):
            #         datetime_timestamp = pd.Timestamp(plc_data_row['timestamp'])
            #     else:
            #         datetime_timestamp = plc_data_row['timestamp'].to_pydatetime()
            #
            #     # takes a local time and change it into UTC
            #     formatted_timestamp = time.mktime(datetime_timestamp.timetuple())
            #     timestamp = datetime.datetime.fromtimestamp(formatted_timestamp)

            key = (plc_data_row['sensor'], plc_data_row['gateway'])
            if key not in sensor_id:
                continue
            s_id = sensor_id[key]
            # Create sensor data model.
            model = plc_data_s.model(
                sensor_id=s_id,
                timestamp=plc_data_row['timestamp'],
                belt_speed=plc_data_row['belt_speed'],
                vfd_current=plc_data_row['vfd_current'],
                vfd_frequency=plc_data_row['vfd_frequency'],
                vfd_voltage=plc_data_row['vfd_voltage'],
                vfd_fault_code=plc_data_row['vfd_fault_code'],
                sorter_induct_rate=plc_data_row['sorter_induct_rate'],
                sorter_reject_rate=plc_data_row['sorter_reject_rate'],
                status=plc_data_row['status'],
                torque=plc_data_row['torque'],
                hours_running=plc_data_row['hours_running'],
                power=plc_data_row['power'],
                rpm=plc_data_row['rpm'],
                horsepower_nameplate=plc_data_row['horsepower_nameplate'],
                voltage_nameplate=plc_data_row['voltage_nameplate'],
                run_speed_nameplate=plc_data_row['run_speed_nameplate'],
                current_nameplate=plc_data_row['current_nameplate'],
                num_phases_nameplate=plc_data_row['num_phases_nameplate']
            )

            models_list.append(model)

        plc_data_s.insert_all(models_list)

        logger.info(f"...sucessfully inserted {len(models_list)} rows of data into '{rds_table_name}' table in RDS")

        if models_list:
            total_inserted += len(models_list)
            logger.debug(f"...sucessfully inserted {len(models_list)} rows of data into PLC table in RDS")
            insert_success_rate = float(total_inserted)/float(len(plc_data))
            logger.info(f"\nSuccessfully migrated {insert_success_rate*100.}% of data\n")
        else:
            logger.info(f"No models created from the {len(plc_data)} data rows retrieved")


def migrate(site_name, athena_table_name, rds_table_name, start_date=datetime.utcnow(), days_back=20, profile=None, rds_host=RDS_HOST, rds_user=RDS_USERNAME):
    """
    migrate data by looping through list of dates
    """
    FILLNA_VALUE = 0.0
    USE_ATHENA = True

    RDS_TABLE_NAME_OPTS = ('plc_data_hour', 'plc_data_minute')
    assert rds_table_name in RDS_TABLE_NAME_OPTS, f"Use RDS Table Name, must be in {RDS_TABLE_NAME_OPTS}"
    assert athena_table_name == 'plc'

    boto_session = boto3.Session(profile_name=profile)
    dates_list = get_dates_list(start_date=start_date, days_back=days_back)
    rds = Connector(boto_session, host=rds_host, username=rds_user)
    schema = site_name.replace('-', '_')
    database, _ = schema.split('_', 1)
    db_session = rds.get_session(database, schema)

    bucket_name = f'{BUCKET_PREFIX}-{site_name}'
    for date in dates_list:
        # use athena for simplicity & safety so we dont have to customize more code
        full_path = f"s3://{bucket_name}/{site_name}/{athena_table_name}/date={date}"
        get_avg_and_insert_plc_data(
                             site_name=site_name,
                             date2query=date,
                             boto_session=boto_session,
                             insert_func=insert_plc_data,
                             db_session=db_session,
                             athena_table_name=athena_table_name,
                             rds_table_name=rds_table_name,
                             use_athena=USE_ATHENA,
                             filepath=full_path,
                             fillna_value=FILLNA_VALUE)


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    log_group = parser.add_mutually_exclusive_group()

    log_group.add_argument('-d', '--debug', action="store_const",
                           help="print debug info", dest="log_level", const=logging.DEBUG,
                           default=logging.INFO)
    log_group.add_argument('-s', '--silent', help="be silent", action="store_const",
                           dest="log_level", const=logging.CRITICAL)
    parser.add_argument('-p', '--profile', help='AWS profile name')
    parser.add_argument('site_name', type=str, help='site to migrate')
    parser.add_argument('table_name', type=str, help='RDS data table name to populate')
    parser.add_argument('days_back', type=int, help='days of data to migrate')

    args = vars(parser.parse_args())
    args['athena_table_name'] = 'plc'

    logging.basicConfig(level=args.pop('log_level'), format=LOCAL_LOG_FORMAT)


    migrate(**args)
