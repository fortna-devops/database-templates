"""
Module for migrating 'mode transition detection' data from S3 to RDS.

..author: Tosin Sonuyi <oluwatosinsonuyi@mhsinc.net>
"""

import argparse
import boto3
from datetime import datetime, timedelta
import logging
import time
from typing import List
from collections import OrderedDict
import csv

import pyarrow.parquet as pq
import s3fs

from mhs_rdb import Connector, ModeTransitionDetections, Conveyors

from migration_utils import get_dates_list, get_then_insert_data, chunks

logger = logging.getLogger()

LOCAL_LOG_FORMAT = '[%(levelname)s][%(module)s] %(asctime)s: %(message)s'
BUCKET_PREFIX = 'mhspredict-site'

RDS_HOST = 'mhspredict-dev.cluster-c2z9g3wxqyud.us-east-1.rds.amazonaws.com'
RDS_USERNAME = 'developer'


def insert_mtd_data(site_name, table_name, data: List[dict], db_session, boto_session: boto3.Session) -> None:
    """
    Insert conveyor_status data into RDS.

    :param conveyor_status_data: sensor data returned from S3
    :type conveyor_status_data: list of dict

    :param boto_session: boto3 session
    :type boto_session: boto3 Session

    :returns: None
    :rtype: None
    """

    logger.info(f'attempting to insert mode_transition_detection data into RDS...')

    conveyors_s = Conveyors(db_session)
    mtd_data_service = ModeTransitionDetections(db_session)

    # Map conveyor_status row to conveyor ID from conveyors table.
    conveyor_id_map = {}
    conveyors = conveyors_s.get_all()
    for conveyor in conveyors:
        unique_id = conveyor.name
        conveyor_id_map[unique_id] = conveyor.id

    total_inserted = 0
    for chunk in chunks(data):
        models_list = []
        for data_row in chunk:

            # Get conveyor ID from conveyors table.
            c_id = conveyor_id_map[data_row['conveyor']]

            # Create MTD data model.
            model = mtd_data_service.model(
                conveyor_id=c_id,
                created_at=data_row['timestamp'],
                transitions=data_row['num_of_transitions']
            )

            models_list.append(model)

        mtd_data_service.insert_all(models_list)

        if models_list:
            total_inserted += len(models_list)
            logger.debug(f"...sucessfully inserted {len(models_list)} rows of data into MTD table in RDS")
            insert_success_rate = float(total_inserted)/float(len(data))
            logger.info(f"\nSuccessfully migrated {insert_success_rate*100.}% of data\n")
        else:
            logger.info(f"No models created from the {len(data)} data rows retrieved")


def migrate(site_name, athena_table_name, start_date=datetime.utcnow(), days_back=20, profile=None, rds_host=RDS_HOST, rds_user=RDS_USERNAME):
    """
    migrate data by looping through list of dates
    """

    boto_session = boto3.Session(profile_name=profile)
    dates_list = get_dates_list(start_date=start_date, days_back=days_back)
    rds = Connector(boto_session, host=rds_host, username=rds_user)
    schema = site_name.replace('-', '_')
    database, _ = schema.split('_', 1)
    db_session = rds.get_session(database, schema)
    bucket_name = f'{BUCKET_PREFIX}-{site_name}'
    for date in dates_list:
        # caution. this table actually uses CSVs not parquet
        # not sure if parquet dataset reading functionwill work here
        # use athena for simplicity & safety so we dont have to customize more code
        full_path = f"s3://{bucket_name}/analytics_output/mode_transition_detection/date={date}"
        get_then_insert_data(bucket_prefix=BUCKET_PREFIX,
                             site_name=site_name,
                             date2query=date,
                             days_back=days_back,
                             boto_session=boto_session,
                             insert_func=insert_mtd_data,
                             db_session=db_session,
                             athena_table_name=athena_table_name,
                             use_athena=True,
                             filepath=full_path)


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--profile', help='AWS profile name')

    log_group = parser.add_mutually_exclusive_group()
    log_group.add_argument('-d', '--debug', action="store_const",
                           help="print debug info", dest="log_level", const=logging.DEBUG,
                           default=logging.INFO)
    log_group.add_argument('-s', '--silent', help="be silent", action="store_const",
                           dest="log_level", const=logging.CRITICAL)

    parser.add_argument('site_name', type=str, help='site to migrate')
    # parser.add_argument('table_name', type=str, help='conveyor_status data table name to migrate')
    parser.add_argument('days_back', type=int, help='days of data to migrate')

    args = parser.parse_args()
    args = vars(args)
    args['athena_table_name'] = 'mode_transition_detection'

    logging.basicConfig(level=args.pop('log_level'), format=LOCAL_LOG_FORMAT)

    logger.info(f"processing {args['site_name']} {args['athena_table_name']} data for migration to RDS")

    boto_session = boto3.Session(profile_name=args['profile'])

    migrate(**args)
