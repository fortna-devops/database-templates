"""
Shared functions for data migrations
..author: Tosin Sonuyi <oluwatosinsonuyi@mhsinc.net>
"""

import boto3
from datetime import datetime, timedelta
import logging
from typing import List

import pyarrow.parquet as pq
import s3fs

from analytics_sdk_core.data_init.data_pull import DataQueryEngineLite as DQE

logger = logging.getLogger()

LOCAL_LOG_FORMAT = '[%(levelname)s][%(module)s] %(asctime)s: %(message)s'


DEFAULT_CHUNK_SIZE = 100


def chunks(l, n=DEFAULT_CHUNK_SIZE):
    """
    Yield successive n-sized chunks from l.
    """
    for i in range(0, len(l), n):
        yield l[i:i + n]


def get_dates_list(start_date: datetime, days_back: int) -> List[str]:
    """
    Create a list of dates in some range starting with the current date.

    :param start_date: the current date
    :type start_date: datetime

    :param days_back: the number of days back for which to get dates
    :type days_back: int

    :returns: list of dates
    :rtype: list
    """

    dates = []
    for n in range(days_back):
        date = start_date - timedelta(days=n)
        formatted_date = date.strftime('%Y-%m-%d')
        dates.append(formatted_date)

    return dates


def get_then_insert_data(
                         # TODO make optional
                         bucket_prefix,
                         # TODO make optional
                         site_name: str, 
                         # TODO make optional
                         date2query: str,
                         # TODO make optional
                         days_back: int,
                         # TODO make optional
                         boto_session: boto3.Session,
                         insert_func,
                         db_session,
                         athena_table_name,
                         use_athena=True,
                         filepath=None) -> List[dict]:

    """
    Get data and then call a generic function for insertion

    :param insert_func: implemented function for specific insertion logic
    :type insert_func: function

    :returns: list of data retrieved from S3
    :rtype: list of dict
    """

    bucket_name = f'{bucket_prefix}-{site_name}'

    fs = s3fs.S3FileSystem(session=boto_session)

    dqe = DQE(bucket_name)

    if use_athena:
        query = f"""SELECT * from {athena_table_name} WHERE date = date '{date2query}';"""
        data = dqe.execute_generic_query(query)

    else:
        logger.info(f'attempting to get {athena_table_name} data from S3 for {date2query}...')

        # https://arrow.apache.org/docs/python/generated/pyarrow.parquet.ParquetDataset.html
        df = pq.ParquetDataset(filepath, filesystem=fs).read_pandas().to_pandas()
        data = df.to_dict('index')

    logger.info(f'...successfully read {len(data)} rows of {athena_table_name} data from data source')

    

    insert_func(site_name=site_name,
                table_name=athena_table_name,
                data=data,
                db_session=db_session,
                boto_session=boto_session)
    
