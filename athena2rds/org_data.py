import codecs
import os
import argparse
import logging
import csv
import re

import boto3
import s3fs
from mhs_rdb import ConfigManager, CustomerSites

logger = logging.getLogger()


LOCAL_LOG_FORMAT = '[%(levelname)s][%(module)s] %(asctime)s: %(message)s'
BUCKET_PREFIX = 'mhspredict-site-'

os.environ['AWS_RDB_USERNAME'] = 'developer'
ENV = 'master'


ORG_DATA_EXTRA_FIELDS = (
    ('Nation', str),
    ('Region', str),
    ('FacilityName', str),
    ('Latitude', float),
    ('Longitude', float)
)


def key_convert(name):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


def get_value(sensor_dict, field_name, d_type=str):
    if field_name not in sensor_dict or sensor_dict[field_name] in ('', 'NULL'):
        value = None
    else:
        value = d_type(sensor_dict[field_name])

    return value


def migrate_customer_sites(customer_name, profile=None):
    logger.info('Migrate "org-data"')
    bucket_name = f'{customer_name}-org-data'
    boto_session = boto3.Session(profile_name=profile)

    fs = s3fs.S3FileSystem(session=boto_session)
    with fs.open(f'{bucket_name}/org_data.csv') as f:
        org_data = list(csv.DictReader(codecs.iterdecode(f, 'utf-8')))

    cm = ConfigManager(ENV)
    schema = 'customer'
    with cm.connector.get_session(customer_name, schema) as session:
        for site in org_data:
            name = site['S3LocationLink'].split('/')[-1].replace(BUCKET_PREFIX, '')
            logger.info(f'Processing site "{name}"')
            extra_fields = {key_convert(f): get_value(site, f, t) for f, t in ORG_DATA_EXTRA_FIELDS}
            session.add(
                CustomerSites.model(name=name, schema_name=name.replace('-', '_'), **extra_fields)
            )
        session.commit()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--profile', help='AWS profile name')

    log_group = parser.add_mutually_exclusive_group()
    log_group.add_argument('-d', '--debug', action="store_const",
                           help="print debug info", dest="log_level", const=logging.DEBUG,
                           default=logging.INFO)
    log_group.add_argument('-s', '--silent', help="be silent", action="store_const",
                           dest="log_level", const=logging.CRITICAL)

    parser.add_argument('customer_name', help='customer to migrate')

    args = parser.parse_args()
    args = vars(args)

    logging.basicConfig(level=args.pop('log_level'), format=LOCAL_LOG_FORMAT)

    migrate_customer_sites(**args)
