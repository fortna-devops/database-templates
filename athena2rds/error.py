"""
Module for migrating 'error' data from S3 to RDS.

..author: Hasan Khan <hasan.khan@mhsglobal.com>
"""

import argparse
import boto3
from datetime import datetime, timedelta
from json import loads
import logging
from sqlalchemy.orm import Session
import time
from typing import List

import pyarrow.parquet as pq
import s3fs

from mhs_rdb import ErrorData, Connector, Sensors
from mhs_rdb.models import ErrorDataModel
import migration_utils

logger = logging.getLogger()

LOCAL_LOG_FORMAT = '[%(levelname)s][%(module)s] %(asctime)s: %(message)s'
BUCKET_PREFIX = 'mhspredict-site'

RDS_HOST = 'mhspredict-dev.cluster-c2z9g3wxqyud.us-east-1.rds.amazonaws.com'
RDS_USERNAME = 'developer'

# TODO: Figure out why this was used. Removing for now.
# def get_sensors_gateways(db_session: Session) -> List[dict]:
#     """
#     Get list of unique sensors and gateway names from 'sensors' table in RDS.

#     :returns: rows from 'sensors' table in RDS
#     :rtype: list of dict
#     """

#     query = f"""SELECT id, gateway_name, name FROM {SCHEMA}.sensors;"""
#     query_res = db_session.execute(query)

#     sensors_gateways = []
#     for row in query_res:
#         sens_gateway_row = {'id': row[0], 'gateway_name': row[1], 'name': row[2]}
#         sensors_gateways.append(sens_gateway_row)

#     return sensors_gateways


def insert_error_data(site_name, table_name, data, db_session, boto_session):
    """
    Insert error data into RDS.

    :param error_data: error data returned from S3
    :type error_data: list of dict

    :param sensors_gateways: unique sensors and gateway names
    :type sensors_gateways: list of dict

    :param boto_session: boto3 session
    :type boto_session: boto3 Session

    :param db_session: SQLAlchemy database Session
    :type db_session: SQLAlchemy database Session

    :returns: None
    :rtype: None
    """

    logger.info(f'attempting to insert error data into RDS...')

    error_s = ErrorData(db_session)

    # TODO: Figure out why this was used. Removing for now.
    # sensor_id = {}
    # for sens_gate in sensors_gateways:
    #     unique_id = (sens_gate['name'], sens_gate['gateway_name'])
    #     sensor_id[unique_id] = sens_gate['id']


    # Map sensor_hour row to sensor ID from sensors table.
    sensors_s = Sensors(db_session)
    sensor_id = {}
    unique_sensors = []
    sensors = sensors_s.get_all()
    for sensor in sensors:
        unique_id = (sensor.name, sensor.gateway_name)
        sensor_id[unique_id] = sensor.id
        if sensor.name not in unique_sensors:
            unique_sensors.append(sensor.name)

    total_inserted = 0
    for chunk in migration_utils.chunks(data):
        models_list = []
        for error_row in chunk:

            # Format timestamp to correct type expected by SQLAlchemy and PostgreSQL.
            # formatted_timestamp = error_row['timestamp']
            # if not isinstance(error_row['timestamp'], int):
            #     datetime_timestamp = error_row['timestamp'].to_pydatetime()
            #     formatted_timestamp = time.mktime(datetime_timestamp.timetuple())

            # timestamp = datetime.fromtimestamp(formatted_timestamp/1000.)

            # Map error data row to sensor ID from sensors table.
            s_id = sensor_id[(error_row['sensor'], error_row['gateway'])]

            # Create error data model.
            model = ErrorDataModel(
                sensor_id=s_id,
                error=error_row['error'],
                timestamp=error_row['timestamp'],
            )

            models_list.append(model)

        error_s.insert_all(models_list)

        if models_list:
            total_inserted += len(models_list)
            logger.info(f"...sucessfully inserted {len(models_list)} rows of data into error_data table in RDS")
            insert_success_rate = float(total_inserted)/float(len(data))
            logger.info(f"\nSuccessfully migrated {insert_success_rate*100.}% of data\n")
        else:
            logger.info(f"No models created from the {len(data)} data rows retrieved")


def migrate(site_name, athena_table_name, start_date=datetime.utcnow(), days_back=20, profile=None, rds_host=RDS_HOST, rds_user=RDS_USERNAME):
    """
    migrate data by looping through list of dates
    """

    dates_list = migration_utils.get_dates_list(start_date=start_date, days_back=days_back)
    boto_session = boto3.Session(profile_name=profile)
    rds = Connector(boto_session, host=rds_host, username=rds_user)
    schema = site_name.replace('-', '_')
    database, _ = schema.split('_', 1)
    db_session = rds.get_session(database, schema)
    bucket_name = f'{BUCKET_PREFIX}-{site_name}'

    for date in dates_list:
        full_path = f"s3://{bucket_name}/{site_name}/{athena_table_name}/date={date}"
        migration_utils.get_then_insert_data(
            bucket_prefix=BUCKET_PREFIX,
            site_name=site_name,
            date2query=date,
            days_back=days_back,
            boto_session=boto_session,
            insert_func=insert_error_data,
            db_session=db_session,
            athena_table_name=athena_table_name,
            use_athena=True,
            filepath=full_path
        )


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--profile', help='AWS profile name')

    log_group = parser.add_mutually_exclusive_group()
    log_group.add_argument('-d', '--debug', action="store_const",
                           help="print debug info", dest="log_level", const=logging.DEBUG,
                           default=logging.INFO)
    log_group.add_argument('-s', '--silent', help="be silent", action="store_const",
                           dest="log_level", const=logging.CRITICAL)

    parser.add_argument('site_name', type=str, help='site to migrate')
    parser.add_argument('days_back', type=int, help='days of data to migrate')

    args = parser.parse_args()
    args = vars(args)
    args['athena_table_name'] = 'error'

    logging.basicConfig(level=args.pop('log_level'), format=LOCAL_LOG_FORMAT)

    logger.info(f"processing {args['site_name']} error data for migration to RDS")

    # Call the main function.
    migrate(**args)
