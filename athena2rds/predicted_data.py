"""
Module for migrating 'predicted_data_minute' and 'predicted_data_hour' data from Athena to RDS.

..author: Hasan Khan <hasan.khan@mhsglobal.com>
"""

import argparse
import boto3
from datetime import datetime, timedelta
import logging
import time
from typing import List

import pyarrow.parquet as pq
import s3fs

from analytics_sdk_core.data_init.data_pull import DataQueryEngineLite as DQE
from mhs_rdb import Connector, PredictedDataHour, PredictedDataMinute, Sensors

from migration_utils import get_dates_list, get_then_insert_data, chunks

logger = logging.getLogger()

LOCAL_LOG_FORMAT = '[%(levelname)s][%(module)s] %(asctime)s: %(message)s'
BUCKET_PREFIX = 'mhspredict-site'

RDS_HOST = 'mhspredict-dev.cluster-c2z9g3wxqyud.us-east-1.rds.amazonaws.com'
RDS_USERNAME = 'developer'


def insert_predicted_data(site_name: str, table_name: str, data: List[dict], db_session, boto_session: boto3.Session) -> None:
    """
    Insert predicted data into RDS.

    :param site_name: the name of the site
    :type site_name: str

    :param table_name: specifies 'predicted_data_minute' or 'predicted_data_hour' table
    :type table_name: str

    :param predicted_data: predicted data returned from S3
    :type predicted_data: list of dict

    :param boto_session: boto3 session
    :type boto_session: boto3 Session

    :returns: None
    :rtype: None
    """

    assert table_name in ['timeseries_predictions', 'timeseries_predictions_hour'], "'timeseries_predictions_hour' and 'timeseries_predictions_minute' are only valid table names"
    
    g_name = site_name.replace('_', '-')

    logger.info(f'attempting to insert {table_name} data into RDS...')

    sensors_s = Sensors(db_session)
    predicted_data_s = PredictedDataHour(db_session) if table_name == 'timeseries_predictions_hour' else PredictedDataMinute(db_session)

    # Map sensor_hour row to sensor ID from sensors table.
    sensor_id = {}
    unique_sensors = []
    sensors = sensors_s.get_all()
    for sensor in sensors:
        unique_id = (sensor.name, sensor.gateway_name)
        sensor_id[unique_id] = sensor.id
        if sensor.name not in unique_sensors:
            unique_sensors.append(sensor.name)

    total_inserted = 0
    for chunk in chunks(data):
        models_list = []
        for pd_row in chunk:

            # Skip sensors that do not exist in the 'sensors' table in Postgres.
            if pd_row['sensor'] not in set(unique_sensors):
                continue

            s_id = sensor_id[(pd_row['sensor'], g_name)]

            try:

                # Create predicted data model.
                model = predicted_data_s.model(
                    sensor_id=s_id,
                    timestamp=pd_row['timestamp'],
                    rms_velocity_x=float(pd_row['rms_velocity_x']),
                    rms_velocity_x_residuals=float(pd_row['rms_velocity_x_residuals']),
                    rms_velocity_z=float(pd_row['rms_velocity_z']),
                    rms_velocity_z_residuals=float(pd_row['rms_velocity_z_residuals']),
                    temperature=float(pd_row['temperature']),
                    temperature_residuals=float(pd_row['temperature_residuals'])
                )

                models_list.append(model)
            except ValueError:
                logger.warning(f'Timeseries Data unable to be converted into ORM Model: {pd_row}\n')
                continue

        predicted_data_s.insert_all(models_list)

        if models_list:
            total_inserted += len(models_list)
            logger.debug(f"...sucessfully inserted {len(models_list)} rows of data into predicted_data table in RDS")
            insert_success_rate = float(total_inserted)/float(len(data))
            logger.info(f"\nSuccessfully migrated {insert_success_rate*100.}% of data\n")
        else:
            logger.info(f"No models created from the {len(data)} data rows retrieved")


def migrate(site_name, athena_table_name, start_date=datetime.utcnow(), days_back=20, profile=None, rds_host=RDS_HOST, rds_user=RDS_USERNAME):
    """
    migrate data by looping through list of dates
    """

    boto_session = boto3.Session(profile_name=profile)
    dates_list = get_dates_list(start_date=start_date, days_back=days_back)
    rds = Connector(boto_session, host=rds_host, username=rds_user)
    schema = site_name.replace('-', '_')
    database, _ = schema.split('_', 1)
    db_session = rds.get_session(database, schema)
    bucket_name = f'{BUCKET_PREFIX}-{site_name}'
    for date in dates_list:
        full_path = f"s3://{bucket_name}/{site_name}/{athena_table_name}/date={date}"
        get_then_insert_data(
            bucket_prefix=BUCKET_PREFIX,
            site_name=site_name,
            date2query=date,
            days_back=days_back,
            boto_session=boto_session,
            insert_func=insert_predicted_data,
            db_session=db_session,
            athena_table_name=athena_table_name,
            use_athena=True,
            filepath=full_path
        )


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--profile', help='AWS profile name')

    log_group = parser.add_mutually_exclusive_group()
    log_group.add_argument('-d', '--debug', action="store_const",
                           help="print debug info", dest="log_level", const=logging.DEBUG,
                           default=logging.INFO)
    log_group.add_argument('-s', '--silent', help="be silent", action="store_const",
                           dest="log_level", const=logging.CRITICAL)

    parser.add_argument('site_name', type=str, help='site to migrate')
    parser.add_argument('table_name', type=str, help='sensor data table name to migrate', dest='athena_table_name')
    parser.add_argument('days_back', type=int, help='days of data to migrate')

    args = parser.parse_args()
    args = vars(args)

    logging.basicConfig(level=args.pop('log_level'), format=LOCAL_LOG_FORMAT)

    logger.info(f"processing {args['site_name']} {args['athena_table_name']} data for migration to RDS")

    boto_session = boto3.Session(profile_name=args['profile'])

    migrate(**args)
