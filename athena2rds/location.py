import json
import logging
import argparse
from datetime import datetime, timedelta

import boto3
from mhs_rdb import Connector, Sensors, Conveyors, Gateways, Thresholds
from analytics_sdk_core.data_init.data_pull import DataQueryEngineLite


logger = logging.getLogger()


LOCAL_LOG_FORMAT = '[%(levelname)s][%(module)s] %(asctime)s: %(message)s'
BUCKET_PREFIX = 'mhspredict-site'


RDS_HOST = 'mhspredict-prod.cluster-c2z9g3wxqyud.us-east-1.rds.amazonaws.com'
RDS_USERNAME = 'developer'


SENSOR_EXTRA_FIELDS = ('description', 'location', 'equipment', 'm_sensor')
RESIDUAL_THRESHOLDS_NAMES = ('low_red', 'low_orange', 'high_orange', 'high_red')


def get_value(sensor_dict, field_name):
    if field_name not in sensor_dict or sensor_dict[field_name] in ('', 'NULL'):
        value = None
    else:
        value = sensor_dict[field_name]

    return value


def guess_type(sensor_dict):
    if 'plc' in sensor_dict['sensor'].lower():
        return Sensors.type.plc
    if 'qm42vt2' in sensor_dict['manufacturer'].lower():
        return Sensors.type.vt
    return None


def get_port(sensor_dict, sensor_info):
    if not sensor_info:
        return None

    for row in sensor_info:
        if row['sensor'] == sensor_dict['sensor']:
            return row['port']

    return None


def migrate_location(site_name, profile=None, rds_host=RDS_HOST, rds_user=RDS_USERNAME):
    logger.info('Migrate "location"')
    bucket_name = f'{BUCKET_PREFIX}-{site_name}'
    boto_session = boto3.Session(profile_name=profile)
    dqe = DataQueryEngineLite(bucket_name, boto_session=boto_session)
    rds = Connector(boto_session, host=rds_host, username=rds_user)
    schema = site_name.replace('-', '_')
    database, _ = schema.split('_', 1)
    db_session = rds.get_session(database, schema)

    location_query = """
        SELECT *
        FROM location
    """
    logger.info('Query "location" table')
    location = dqe.execute_generic_query(location_query)

    logger.info('Get sensor info')
    dt = datetime.utcnow().date()
    end_date = dt.strftime('%Y-%m-%d')
    start_date = (dt - timedelta(days=10)).strftime('%Y-%m-%d')
    data_query_template = """
        SELECT gateway,port,sensor 
        FROM "{table_name}"
        WHERE date BETWEEN date '{start_date}' AND date '{end_date}'
        GROUP BY (gateway,port,sensor)
    """

    sensor_info_map = {
        Sensors.type.vt: dqe.execute_generic_query(data_query_template.format(table_name='sensor_hour', start_date=start_date, end_date=end_date)),
        Sensors.type.plc: dqe.execute_generic_query(data_query_template.format(table_name='plc', start_date=start_date, end_date=end_date))
    }

    sensors_s = Sensors(db_session)
    conveyors_s = Conveyors(db_session)
    gateway_s = Gateways(db_session)
    thresholds_s = Thresholds(db_session)

    logger.info(f'Creating gateway "{site_name}"')
    gateway = gateway_s.model(name=site_name)
    gateway_s.insert(gateway)

    seen_conveyors = {}

    for a_sensor in location:
        logger.info(f'Processing sensor "{a_sensor["sensor"]}"')
        a_conveyor = get_value(a_sensor, 'conveyor')
        if a_conveyor is None:
            conveyor_id = None
        elif a_conveyor in seen_conveyors:
            conveyor_id = seen_conveyors[a_conveyor]
        else:
            logger.info(f'Creating conveyor "{a_conveyor}"')
            conveyor = conveyors_s.model(name=a_conveyor, criticality=Conveyors.criticality.medium)
            conveyors_s.insert(conveyor)
            conveyor_id = conveyor.id
            seen_conveyors[a_conveyor] = conveyor_id

        extra_fields = {f: get_value(a_sensor, f) for f in SENSOR_EXTRA_FIELDS}

        logger.info(f'Creating sensor "{a_sensor["sensor"]}"')
        sensor_type = guess_type(a_sensor)
        sensor = sensors_s.model(
            gateway_name=gateway.name, conveyor_id=conveyor_id, name=a_sensor['sensor'],
            manufacturer=a_sensor['manufacturer'], standards_score=a_sensor['standards_score'],
            facility_organization=a_sensor['facility_organization'], type=sensor_type,
            port=get_port(a_sensor, sensor_info_map.get(sensor_type)),
            criticality=Sensors.criticality.medium, **extra_fields
        )
        sensors_s.insert(sensor)

        logger.info('Processing thresholds')
        a_thresholds = json.loads(a_sensor['thresholds'])
        for metric, thresholds in a_thresholds.items():
            for th_name, th_value in thresholds.items():
                thresholds_s.insert(thresholds_s.model(
                    sensor_id=sensor.id, type=thresholds_s.type.ISO,
                    metric=getattr(thresholds_s.metric, metric), level=thresholds_s.level.equipment,
                    name=th_name, value=th_value
                ))

        logger.info('Processing residual thresholds')
        a_residual_thresholds = json.loads(a_sensor['residual_thresholds'])
        for metric, thresholds in a_residual_thresholds.items():
            metric = metric.replace('_residuals', '')
            for th_name, th_value in zip(RESIDUAL_THRESHOLDS_NAMES, thresholds):
                thresholds_s.insert(thresholds_s.model(
                    sensor_id=sensor.id, type=thresholds_s.type.RESIDUAL,
                    metric=getattr(thresholds_s.metric, metric), level=thresholds_s.level.equipment,
                    name=th_name, value=th_value
                ))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--profile', help='AWS profile name')

    log_group = parser.add_mutually_exclusive_group()
    log_group.add_argument('-d', '--debug', action="store_const",
                           help="print debug info", dest="log_level", const=logging.DEBUG,
                           default=logging.INFO)
    log_group.add_argument('-s', '--silent', help="be silent", action="store_const",
                           dest="log_level", const=logging.CRITICAL)

    parser.add_argument('site_name', help='site to migrate')

    args = parser.parse_args()
    args = vars(args)

    logging.basicConfig(level=args.pop('log_level'), format=LOCAL_LOG_FORMAT)

    migrate_location(**args)
