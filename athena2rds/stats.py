"""
Module for migrating 'stats' data from S3 to RDS.

..author: Hasan Khan <hasan.khan@mhsglobal.com>
"""

import argparse
import boto3
from collections import OrderedDict
import csv
from datetime import datetime, timedelta
import logging
import time
from typing import List, Optional

import pyarrow.parquet as pq
import s3fs

from mhs_rdb import Stats, Sensors, Connector
from mhs_rdb.models import StatsModel

import migration_utils

logger = logging.getLogger()

LOCAL_LOG_FORMAT = '[%(levelname)s][%(module)s] %(asctime)s: %(message)s'
BUCKET_PREFIX = 'mhspredict-site'

RDS_HOST = 'mhspredict-dev.cluster-c2z9g3wxqyud.us-east-1.rds.amazonaws.com'
RDS_USERNAME = 'developer'


def get_stats_data(site_name: str, boto_session: boto3.Session) -> List[OrderedDict]:
    """
    Get stats data from S3.

    :param site_name: the name of the site
    :type site_name: str

    :param boto_session: boto3 session
    :type boto_session: boto3 Session

    :returns: list of stats data retrieved from S3
    :rtype: list of OrderedDict
    """

    bucket_name = f'{BUCKET_PREFIX}-{site_name}'

    fs = s3fs.S3FileSystem(session=boto_session)

    logger.info(f'attempting to get stats data from S3 for {site_name}...')

    stats_data = []

    # Read data from S3.
    path = f's3://{bucket_name}/{site_name}/stats'
    for f in fs.ls(path):

        with fs.open(f, 'rb') as fl:
            data = fl.read().decode("utf-8").split('\n')
            reader = csv.DictReader(data, delimiter=',', quotechar='"')
            for row in reader:
                row['gateway'] = site_name
                stats_data.append(row)

    logger.info(f'...successfully returned {len(stats_data)} files of stats data from S3')

    return stats_data


def insert_stats_data(stats_data: List[OrderedDict], boto_session: boto3.Session, db_session) -> None:
    """
    Insert stats data into RDS.

    :param stats_data: stats data returned from S3
    :type stats_data: list of OrderedDict

    :param boto_session: boto3 session
    :type boto_session: boto3 Session

    :returns: None
    :rtype: None
    """

    logger.info(f'attempting to insert stats data into RDS...')

    stats_s = Stats(db_session)
    sensors_s = Sensors(db_session)

    # Map sensor data row to sensor ID from sensors table.
    sensor_id = {}
    sensors = sensors_s.get_all()
    for sensor in sensors:
        unique_id = (sensor.name, sensor.gateway_name)
        sensor_id[unique_id] = sensor.id

    total_inserted = 0
    for chunk in migration_utils.chunks(stats_data):
        models_list = []
        for s in chunk:

            # Get sensor ID from sensors table.
            s_id = sensor_id[(s['sensor'], s['gateway'])]

            stats_model = StatsModel(
                sensor_id=s_id,
                temperature_mean=s['temperature_mean'],
                temperature_perc95=s['temperature_perc95'],
                rms_velocity_z_mean=s['rms_velocity_z_mean'],
                rms_velocity_z_perc95=s['rms_velocity_z_perc95'],
                hf_rms_acceleration_x_mean=s['hf_rms_acceleration_x_mean'],
                hf_rms_acceleration_x_perc95=s['hf_rms_acceleration_x_perc95'],
                hf_rms_acceleration_z_mean=s['hf_rms_acceleration_z_mean'],
                hf_rms_acceleration_z_perc95=s['hf_rms_acceleration_z_perc95'],
                rms_velocity_x_mean=s['rms_velocity_x_mean'],
                rms_velocity_x_perc95=s['rms_velocity_x_perc95']
            )

            models_list.append(stats_model)

        stats_s.insert_all(models_list)

        if models_list:
            total_inserted += len(models_list)
            logger.debug(f"...sucessfully inserted {len(models_list)} rows of data into stats table in RDS")
            insert_success_rate = float(total_inserted)/float(len(stats_data))
            logger.info(f"\nSuccessfully migrated {insert_success_rate*100.}% of data\n")
        else:
            logger.info(f"No models created from the {len(stats_data)} data rows retrieved")


def migrate(site_name, profile=None, rds_host=RDS_HOST, rds_user=RDS_USERNAME):
    # Call the main functions.
    boto_session = boto3.Session(profile_name=profile)
    rds = Connector(boto_session, host=rds_host, username=rds_user)
    schema = site_name.replace('-', '_')
    database, _ = schema.split('_', 1)
    db_session = rds.get_session(database, schema)
    stats_data = get_stats_data(
        site_name=site_name,
        boto_session=boto_session
    )
    insert_stats_data(stats_data, boto_session=boto_session, db_session=db_session)


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--profile', help='AWS profile name')

    log_group = parser.add_mutually_exclusive_group()
    log_group.add_argument('-d', '--debug', action="store_const",
                           help="print debug info", dest="log_level", const=logging.DEBUG,
                           default=logging.INFO)
    log_group.add_argument('-s', '--silent', help="be silent", action="store_const",
                           dest="log_level", const=logging.CRITICAL)

    parser.add_argument('site_name', type=str, help='site to migrate')

    args = parser.parse_args()
    args = vars(args)

    logging.basicConfig(level=args.pop('log_level'), format=LOCAL_LOG_FORMAT)

    logger.info(f"processing {args['site_name']} stats data for migration to RDS")

    migrate(**args)
