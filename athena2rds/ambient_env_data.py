"""
Module for migrating ambient env data from S3 to RDS.

..author: Hasan Khan <hasan.khan@mhsglobal.com>
"""

import argparse
import boto3
from datetime import datetime, timedelta
import logging
import time
from typing import List

import pyarrow.parquet as pq
import s3fs

from analytics_sdk_core.data_init.data_pull import DataQueryEngineLite as DQE
from mhs_rdb import Connector, SensorDataHour, SensorDataMinute, AmbientEnvData, Sensors
from mhs_rdb.models import AmbientEnvDataModel
import migration_utils

logger = logging.getLogger()

LOCAL_LOG_FORMAT = '[%(levelname)s][%(module)s] %(asctime)s: %(message)s'
BUCKET_PREFIX = 'mhspredict-site'

RDS_HOST = 'mhspredict-dev.cluster-c2z9g3wxqyud.us-east-1.rds.amazonaws.com'
RDS_USERNAME = 'developer'


def insert_ambient_env_data(site_name, table_name, data, db_session, boto_session) -> None:
    """
    Insert ambient env data into RDS.

    :param ambient_env_data: sensor data returned from S3
    :type ambient_env_data: list of dict

    :param boto_session: boto3 session
    :type boto_session: boto3 Session

    :returns: None
    :rtype: None
    """

    logger.info(f'attempting to insert ambient env data into RDS...')

    ambient_env_data_s = AmbientEnvData(db_session)
    sensors_s = Sensors(db_session)

    # Map ambient_env row to sensor ID from sensors table.
    sensor_id = {}
    sensors = sensors_s.get_all()
    for sensor in sensors:
        unique_id = (sensor.name, sensor.gateway_name)
        sensor_id[unique_id] = sensor.id

    total_inserted = 0
    for chunk in migration_utils.chunks(data):
        models_list = []
        for ambient_env_row in chunk:

            # Get sensor ID from sensors table.
            s_id = sensor_id[(ambient_env_row['sensor'], ambient_env_row['gateway'])]

            # Create sensor data model.
            model = AmbientEnvDataModel(
                sensor_id=s_id,
                timestamp=ambient_env_row['timestamp'],
                humidity=ambient_env_row['humidity'],
                temperature=ambient_env_row['temperature']
            )

            models_list.append(model)

        ambient_env_data_s.insert_all(models_list)

    
        if models_list:
            total_inserted += len(models_list)
            logger.info(f"...sucessfully inserted {len(models_list)} rows of data into 'ambient_env_data' table in RDS")
            insert_success_rate = float(total_inserted)/float(len(data))
            logger.info(f"\nSuccessfully migrated {insert_success_rate*100.}% of data\n")
        else:
            logger.info(f"No models created from the {len(data)} data rows retrieved")


def migrate(site_name, athena_table_name, start_date=datetime.utcnow(), days_back=20, profile=None, rds_host=RDS_HOST, rds_user=RDS_USERNAME):
    """
    migrate data by looping through list of dates
    """

    dates_list = migration_utils.get_dates_list(start_date=start_date, days_back=days_back)
    boto_session = boto3.Session(profile_name=profile)
    rds = Connector(boto_session, host=rds_host, username=rds_user)
    schema = site_name.replace('-', '_')
    database, _ = schema.split('_', 1)
    db_session = rds.get_session(database, schema)
    bucket_name = f'{BUCKET_PREFIX}-{site_name}'
    for date in dates_list:
        full_path = f"s3://{bucket_name}/{site_name}/{athena_table_name}/date={date}"
        migration_utils.get_then_insert_data(bucket_prefix=BUCKET_PREFIX,
                            site_name=site_name,
                            date2query=date,
                            days_back=days_back,
                            boto_session=boto_session,
                            insert_func=insert_ambient_env_data,
                            db_session=db_session,
                            athena_table_name=athena_table_name,
                            use_athena=True,
                            filepath=full_path)


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--profile', help='AWS profile name')

    log_group = parser.add_mutually_exclusive_group()
    log_group.add_argument('-d', '--debug', action="store_const",
                           help="print debug info", dest="log_level", const=logging.DEBUG,
                           default=logging.INFO)
    log_group.add_argument('-s', '--silent', help="be silent", action="store_const",
                           dest="log_level", const=logging.CRITICAL)

    parser.add_argument('site_name', type=str, help='site to migrate')
    parser.add_argument('days_back', type=int, help='days of data to migrate')

    args = parser.parse_args()
    args = vars(args)

    args['athena_table_name'] = 'ambient_env'
    logging.basicConfig(level=args.pop('log_level'), format=LOCAL_LOG_FORMAT)

    logger.info(f"processing {args['site_name']} ambient env data for migration to RDS")

    # Get and insert ambient env data.
    migrate(**args)
