import logging.config


def configure_logs(log_level, site_name):
    logging_config = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format': '[%(levelname)s][%(module)s] %(asctime)s: %(message)s'
            },
        },
        'handlers': {
            'console': {
                'level': 'DEBUG',
                'formatter': 'standard',
                'class': 'logging.StreamHandler'
            },
            'info_file': {
                'level': 'INFO',
                'formatter': 'standard',
                'class': 'logging.FileHandler',
                'filename': f'info-{site_name}.log',
            },
            'error_file': {
                'level': 'ERROR',
                'formatter': 'standard',
                'class': 'logging.FileHandler',
                'filename': f'error-{site_name}.log',
            }
        },
        'loggers': {
            '': {
                'handlers': ['console', 'info_file', 'error_file'],
                'level': log_level,
                'propagate': True
            }
        }
    }

    logging.config.dictConfig(logging_config)
