#!/usr/bin/env bash

set -e

SITES="fedex-louisville dhl-miami dhl-milano dhl-cincinnati amazon-sdf4 ups-independence"

for site_name in ${SITES}
do
	python3 main.py ${site_name}
done
