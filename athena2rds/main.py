#!/usr/bin/env python3

import argparse
import logging
import multiprocessing
from datetime import datetime

from location import migrate_location
from alarms import migrate as migrate_alarms
from conveyor_status_data import migrate as migrate_conveyor_status_data
from alert_user_plus_data import migrate as migrate_alert
from ambient_env_data import migrate as migrate_ambient_env_data
from error import migrate as migrate_error_data
from mode_transition_detections import migrate as migrate_mtd
from plc2rds import migrate as migrate_plc_data
from predicted_data import migrate as migrate_predicted_data
from sensor_data import migrate as migrate_sensor_data
from stats import migrate as migrate_stats


from log_config import configure_logs


logger = logging.getLogger()


RDS_HOSTNAME = 'mhspredict-prod.cluster-c2z9g3wxqyud.us-east-1.rds.amazonaws.com'
RDS_USERNAME = 'developer'
DAYS_BACK = 60
START_DATE = datetime(year=2019, month=12, day=16)

class Task:

    def __init__(self, name, func, is_async=True, **f_kwargs):
        self.name = name
        self.func = func
        self.is_async = is_async
        self.f_kwargs = f_kwargs

    def __call__(self, **kwargs):
        logger.info(f'running "{self.name}"')
        try:
            self.func(**dict(self.f_kwargs, **kwargs))
        except:
            logger.exception(f'An error occurred during {self.name}')
            raise


# WARNING: sync tasks should be placed before async
TASKS = [
    Task('migrate_location', migrate_location, is_async=False),
    Task('migrate_alert', migrate_alert),
    Task('migrate_alarms', migrate_alarms, athena_table_name='alarms', start_date=START_DATE, days_back=DAYS_BACK),
    Task('migrate_conveyor_status_data', migrate_conveyor_status_data, athena_table_name='conveyor_status', start_date=START_DATE, days_back=DAYS_BACK),

    Task('migrate_stats', migrate_stats),
    Task('migrate_mtd', migrate_mtd, athena_table_name='mode_transition_detection', start_date=START_DATE, days_back=DAYS_BACK),

    Task('migrate_ambient_env_data', migrate_ambient_env_data, athena_table_name='ambient_env', start_date=START_DATE, days_back=DAYS_BACK),
    Task('migrate_error_data', migrate_error_data, athena_table_name='error', start_date=START_DATE, days_back=DAYS_BACK),
    #
    Task('migrate_sensor_data_minute', migrate_sensor_data, athena_table_name='sensor_minute', start_date=START_DATE, days_back=DAYS_BACK),
    Task('migrate_sensor_data_hour', migrate_sensor_data, athena_table_name='sensor_hour', start_date=START_DATE, days_back=DAYS_BACK),
    #
    Task('migrate_predicted_data_minute', migrate_predicted_data, athena_table_name='timeseries_predictions', start_date=START_DATE, days_back=DAYS_BACK),
    Task('migrate_predicted_data_hour', migrate_predicted_data, athena_table_name='timeseries_predictions_hour', start_date=START_DATE, days_back=DAYS_BACK),

    Task('migrate_plc_data_minute', migrate_plc_data, athena_table_name='plc', rds_table_name='plc_data_minute', start_date=START_DATE, days_back=DAYS_BACK),
    Task('migrate_plc_data_hour', migrate_plc_data, athena_table_name='plc', rds_table_name='plc_data_hour', start_date=START_DATE, days_back=DAYS_BACK),
]


def run_migrations(site_name, processes, rdb_host, rdb_user,  profile):
    pool = multiprocessing.Pool(processes=processes)
    async_jobs = []
    kwargs = {
        'site_name': site_name,
        'profile': profile,
        'rds_host': rdb_host,
        'rds_user': rdb_user,
    }
    for task in TASKS:
        if task.is_async:
            async_jobs.append(pool.apply_async(task, kwds=kwargs))
        else:
            pool.apply(task, kwds=kwargs)

    pool.close()
    pool.join()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--profile', help='AWS profile name')
    parser.add_argument('-j', '--processes', type=int, default=None,
                        help=f'number of concurrent processes '
                             f'(default equals to cpu_count)')

    log_group = parser.add_mutually_exclusive_group()
    log_group.add_argument('-d', '--debug', action="store_const",
                           help="print debug info", dest="log_level", const=logging.DEBUG,
                           default=logging.INFO)
    log_group.add_argument('-s', '--silent', help="be silent", action="store_const",
                           dest="log_level", const=logging.CRITICAL)

    parser.add_argument('--rdb_host', help=f'RDB hostname (default is {RDS_HOSTNAME})',
                        default=RDS_HOSTNAME)
    parser.add_argument('--rdb_user', help=f'RDB username (default is {RDS_USERNAME})',
                        default=RDS_USERNAME)

    parser.add_argument('site_name', help='site to migrate')

    args = parser.parse_args()
    args = vars(args)

    configure_logs(args.pop('log_level'), site_name=args['site_name'])

    run_migrations(**args)
