"""
Module for migrating 'sensor_hour' and 'sensor_minute' data from S3 to RDS.

..author: Hasan Khan <hasan.khan@mhsglobal.com>
..author: Tosin Sonuyi <oluwatosinsonuyi@mhsinc.net>
"""

import argparse
import boto3
from datetime import datetime, timedelta
import logging
import time
from typing import List

import pyarrow.parquet as pq
import s3fs

from mhs_rdb import Connector, SensorDataHour, SensorDataMinute, Sensors

from migration_utils import get_dates_list, get_then_insert_data, chunks

logger = logging.getLogger()

LOCAL_LOG_FORMAT = '[%(levelname)s][%(module)s] %(asctime)s: %(message)s'
BUCKET_PREFIX = 'mhspredict-site'

RDS_HOST = 'mhspredict-prod.cluster-c2z9g3wxqyud.us-east-1.rds.amazonaws.com'
RDS_USERNAME = 'developer'


def insert_sensor_data(site_name, table_name: str, data: List[dict], db_session, boto_session: boto3.Session) -> None:
    """
    Insert sensor data into RDS.

    :param table_name: specifies 'sensor_hour' or 'sensor_minute' table
    :type table_name: str

    :param sensor_data: sensor data returned from S3
    :type sensor_data: list of dict

    :param boto_session: boto3 session
    :type boto_session: boto3 Session

    :returns: None
    :rtype: None
    """

    assert table_name in ['sensor_hour', 'sensor_minute'], "'sensor_hour' and 'sensor_minute' are only valid table names"

    logger.info(f'attempting to insert {table_name} data into RDS...')

    sensors_s = Sensors(db_session)
    sensor_data_s = SensorDataHour(db_session) if table_name == 'sensor_hour' else SensorDataMinute(db_session)

    # Map sensor_hour row to sensor ID from sensors table.
    sensor_id = {}
    sensors = sensors_s._get_all()
    for sensor in sensors:
        unique_id = (sensor.name, sensor.gateway_name)
        sensor_id[unique_id] = sensor.id

        # Special case for ups
        unique_id = (sensor.name, sensor.gateway_name.split(':')[0])
        sensor_id[unique_id] = sensor.id


    total_inserted = 0
    for chunk in chunks(data):
        data_models = []
        for data_row in chunk:

            # Format timestamp to correct type expected by SQLAlchemy and PostgreSQL.
            # formatted_timestamp = data_row['timestamp']
            # if not isinstance(data_row['timestamp'], int):
            #     datetime_timestamp = data_row['timestamp'].to_pydatetime()
            #     formatted_timestamp = time.mktime(datetime_timestamp.timetuple())

            # timestamp = datetime.fromtimestamp(formatted_timestamp/1000.)

            # Get sensor ID from sensors table.
            key = (data_row['sensor'], data_row['gateway'])
            if key not in sensor_id:
                continue
            s_id = sensor_id[key]

            # TODO: timestamps need to be converted to UTC before insertion
            # because timestamp column is without timezone.
            # i also think this is dependent on the region you are querying from.

            # Create sensor data model.
            model = sensor_data_s.model(
                sensor_id=s_id,
                timestamp=data_row['timestamp'],
                rms_velocity_z=data_row['rms_velocity_z'],
                temperature=data_row['temperature'],
                rms_velocity_x=data_row['rms_velocity_x'],
                peak_acceleration_z=data_row['peak_acceleration_z'],
                peak_acceleration_x=data_row['peak_acceleration_x'],
                peak_frequency_z=data_row['peak_frequency_z'],
                peak_frequency_x=data_row['peak_frequency_x'],
                rms_acceleration_z=data_row['rms_acceleration_z'],
                rms_acceleration_x=data_row['rms_acceleration_x'],
                kurtosis_z=data_row['kurtosis_z'],
                kurtosis_x=data_row['kurtosis_x'],
                crest_acceleration_z=data_row['crest_acceleration_z'],
                crest_acceleration_x=data_row['crest_acceleration_x'],
                peak_velocity_z=data_row['peak_velocity_z'],
                peak_velocity_x=data_row['peak_velocity_x'],
                hf_rms_acceleration_z=data_row['hf_rms_acceleration_z'],
                hf_rms_acceleration_x=data_row['hf_rms_acceleration_x']
            )

            data_models.append(model)

        sensor_data_s.insert_all(data_models)

        if data_models:
            total_inserted += len(data_models)
            logger.debug(f"...sucessfully inserted {len(data_models)} rows of data into sensor data table in RDS")
            insert_success_rate = float(total_inserted)/float(len(data))
            logger.info(f"\nSuccessfully migrated {insert_success_rate*100.}% of data\n")
        else:
            logger.info(f"No models created from the {len(data)} data rows retrieved")


def migrate(site_name, athena_table_name, start_date=datetime.utcnow(), days_back=20, profile=None, rds_host=RDS_HOST, rds_user=RDS_USERNAME):
    """
    migrate data by looping through list of dates
    """

    boto_session = boto3.Session(profile_name=profile)
    dates_list = get_dates_list(start_date=start_date, days_back=days_back)
    rds = Connector(boto_session, host=rds_host, username=rds_user)
    schema = site_name.replace('-', '_')
    database, _ = schema.split('_', 1)
    db_session = rds.get_session(database, schema)
    bucket_name = f'{BUCKET_PREFIX}-{site_name}'
    for date in dates_list:
        full_path = f"s3://{bucket_name}/{site_name}/{athena_table_name}/date={date}"
        get_then_insert_data(bucket_prefix=BUCKET_PREFIX,
                             site_name=site_name,
                             date2query=date,
                             days_back=days_back,
                             boto_session=boto_session,
                             insert_func=insert_sensor_data,
                             db_session=db_session,
                             athena_table_name=athena_table_name,
                             use_athena=True,
                             filepath=full_path)


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--profile', help='AWS profile name')

    log_group = parser.add_mutually_exclusive_group()
    log_group.add_argument('-d', '--debug', action="store_const",
                           help="print debug info", dest="log_level", const=logging.DEBUG,
                           default=logging.INFO)
    log_group.add_argument('-s', '--silent', help="be silent", action="store_const",
                           dest="log_level", const=logging.CRITICAL)

    parser.add_argument('site_name', type=str, help='site to migrate')
    parser.add_argument('athena_table_name', type=str, help='sensor data table name to migrate')
    parser.add_argument('days_back', type=int, help='days of data to migrate')

    args = parser.parse_args()
    args = vars(args)

    logging.basicConfig(level=args.pop('log_level'), format=LOCAL_LOG_FORMAT)

    logger.info(f"processing {args['site_name']} {args['athena_table_name']} data for migration to RDS")
    
    migrate(**args)
