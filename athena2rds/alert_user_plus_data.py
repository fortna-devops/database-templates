"""
Module for migrating 'alert_user' and 'alert_user_data' data from S3 to RDS.

..author: Tosin Sonuyi <oluwatosinsonuyi@mhsinc.net>
"""

import json
import logging
import argparse

import boto3
from mhs_rdb import Connector, Sensors, AlertUsers, AlertBlackList, AlertRoles
from mhs_rdb.models import AlertUsersModel, AlertRolesModel, AlertBlackListModel, AlertRolesEnum
from analytics_sdk_core.data_init.data_pull import DataQueryEngineLite


logger = logging.getLogger()


LOCAL_LOG_FORMAT = '[%(levelname)s][%(module)s] %(asctime)s: %(message)s'
BUCKET_PREFIX = 'mhspredict-site'


RDS_HOST = 'mhspredict-dev.cluster-c2z9g3wxqyud.us-east-1.rds.amazonaws.com'
RDS_USERNAME = 'developer'


def get_value(sensor_dict, field_name):
    if field_name not in sensor_dict or sensor_dict[field_name] in ('', 'NULL'):
        value = None
    else:
        value = sensor_dict[field_name]

    return value


def migrate(site_name, profile=None, rds_host=RDS_HOST, rds_user=RDS_USERNAME):
    bucket_name = f'{BUCKET_PREFIX}-{site_name}'
    boto_session = boto3.Session(profile_name=profile)
    dqe = DataQueryEngineLite(bucket_name, boto_session=boto_session)
    rds = Connector(boto_session, host=rds_host, username=rds_user)
    schema = site_name.replace('-', '_')
    database, _ = schema.split('_', 1)
    db_session = rds.get_session(database, schema)

    alert_users_query = """
        SELECT *
        FROM alert_users
    """
    logger.info('Query "alert_users" table')
    alert_user_data = dqe.execute_generic_query(alert_users_query)

    sensors_s = Sensors(db_session)
    alert_user_s = AlertUsers(db_session)
    alert_black_list_s = AlertBlackList(db_session)
    alert_roles_s = AlertRoles(db_session)
    # Map sensor_hour row to sensor ID from sensors table.
    sensor_id_map = {}
    sensors = sensors_s.get_all()
    for sensor in sensors:
        unique_id = (sensor.name)
        sensor_id_map[unique_id] = sensor.id

    alert_role_enum_map = {
        'developer': AlertRolesEnum.developer,
        'support': AlertRolesEnum.support,
        'customer': AlertRolesEnum.customer
    }

    alert_user_models = []
    alert_role_models = []
    alert_black_list_models = []

    for row in alert_user_data:

        logger.info('Processing alert user rows and data')

        isActive = row['active'].upper() == 'TRUE'

        # create the users
        alert_user_model = AlertUsersModel(
                email=row['email'],
                phone=row['phone'],
                active=isActive
            )
        alert_user_models.append(alert_user_model)

        current_user_roles = row['roles'].split(':')

        # create the user roles
        for current_user_role in current_user_roles:

            enum_role = alert_role_enum_map[current_user_role]
            alert_role_model = AlertRolesModel(
                role=enum_role,
                alert_user_email=row['email']
                )

            alert_role_models.append(alert_role_model)


    # making a separate query for simplicity/readability instead of a join
    # its tiny amount of data
    alert_users_data_query = f"SELECT * FROM alert_users_data"
    alert_user_data_result = dqe.execute_generic_query(alert_users_data_query)

    for user_data_row in alert_user_data_result:

        # conver the user blacklist as dict
        blacklist_str = user_data_row['sensor_black_list']
        blacklist_dict = json.loads(blacklist_str)
        user_email = user_data_row['email']
        for original_sensor_name, created_tstamp_dict in blacklist_dict.items():
            sensor_pk_id = sensor_id_map[(original_sensor_name)]
            # the timezeone returned from the query is a string in UTC
            created_tstamp = created_tstamp_dict['create_datetime']
            alert_black_list_model = AlertBlackListModel(
                sensor_id=sensor_pk_id,
                alert_user_email=user_email,
                created_at=created_tstamp
                )
            alert_black_list_models.append(alert_black_list_model)


    logger.info(f'Inserting {len(alert_user_models)} AlertUser rows')
    alert_user_s.insert_all(alert_user_models)

    logger.info(f'Inserting {len(alert_role_models)} AlertRole rows')
    alert_roles_s.insert_all(alert_role_models)

    logger.info(f'Inserting {len(alert_black_list_models)} AlertBlackList rows')
    alert_black_list_s.insert_all(alert_black_list_models)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--profile', help='AWS profile name')

    log_group = parser.add_mutually_exclusive_group()
    log_group.add_argument('-d', '--debug', action="store_const",
                           help="print debug info", dest="log_level", const=logging.DEBUG,
                           default=logging.INFO)
    log_group.add_argument('-s', '--silent', help="be silent", action="store_const",
                           dest="log_level", const=logging.CRITICAL)

    parser.add_argument('site_name', help='site to migrate')

    args = parser.parse_args()
    args = vars(args)

    logging.basicConfig(level=args.pop('log_level'), format=LOCAL_LOG_FORMAT)

    migrate(**args)
