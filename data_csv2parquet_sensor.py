import argparse
import logging
import datetime
import multiprocessing
import time
from functools import partial

from dateutil.relativedelta import relativedelta

import s3fs
import pyarrow as pa
import pyarrow.parquet as pq
import pandas as pd
import numpy as np


logger = logging.getLogger()


BUCKET_NAME = 'mhspredict-site-dhl-miami'
SITE_NAME = 'dhl-miami'
TABLE_NAME = 'sensor'

HUMANIZE_TIME_ATTRS = ['years', 'months', 'days', 'hours', 'minutes', 'seconds']

DATA_PATH = 's3://{}/{}/data/'.format(BUCKET_NAME, TABLE_NAME)


PYARROW_SCHEMA = pa.schema([
    pa.field('timestamp', pa.timestamp('ms')),
    pa.field('gateway', pa.string()),
    pa.field('port', pa.string()),
    pa.field('sensor', pa.string()),
    pa.field('rms_velocity_z', pa.float64()),
    pa.field('temperature', pa.float64()),
    pa.field('rms_velocity_x', pa.float64()),
    pa.field('peak_acceleration_z', pa.float64()),
    pa.field('peak_acceleration_x', pa.float64()),
    pa.field('peak_frequency_z', pa.float64()),
    pa.field('peak_frequency_x', pa.float64()),
    pa.field('rms_acceleration_z', pa.float64()),
    pa.field('rms_acceleration_x', pa.float64()),
    pa.field('kurtosis_z', pa.float64()),
    pa.field('kurtosis_x', pa.float64()),
    pa.field('crest_acceleration_z', pa.float64()),
    pa.field('crest_acceleration_x', pa.float64()),
    pa.field('peak_velocity_z', pa.float64()),
    pa.field('peak_velocity_x', pa.float64()),
    pa.field('hf_rms_acceleration_z', pa.float64()),
    pa.field('hf_rms_acceleration_x', pa.float64()),
])


PANDAS_SCHEMA = {
    'gateway': str,
    'port': str,
    'sensor': str,
    'rms_velocity_z': np.float64,
    'temperature': np.float64,
    'rms_velocity_x': np.float64,
    'peak_acceleration_z': np.float64,
    'peak_acceleration_x': np.float64,
    'peak_frequency_z': np.float64,
    'peak_frequency_x': np.float64,
    'rms_acceleration_z': np.float64,
    'rms_acceleration_x': np.float64,
    'kurtosis_z': np.float64,
    'kurtosis_x': np.float64,
    'crest_acceleration_z': np.float64,
    'crest_acceleration_x': np.float64,
    'peak_velocity_z': np.float64,
    'peak_velocity_x': np.float64,
    'hf_rms_acceleration_z': np.float64,
    'hf_rms_acceleration_x': np.float64
}


def humanize_timedelta(delta):
    res = []
    for attr in HUMANIZE_TIME_ATTRS:
        value = getattr(delta, attr)
        if not value:
            continue

        if value == 1:
            attr = attr[:-1]

        res.append('{} {}'.format(value, attr))

    return ' '.join(res)


def get_time_delta(t1, t2, humanize=True):
    delta = relativedelta(
        datetime.datetime.fromtimestamp(t1),
        datetime.datetime.fromtimestamp(t2)
    )

    if humanize:
        delta = humanize_timedelta(delta)

    return delta


def write_dataset(group, fs, table_name):
    key, data = group
    logger.info('Writing {} file for {:%Y-%m-%d %H:%M}'.format(table_name, key))
    data = pa.Table.from_pandas(data.reset_index(), schema=PYARROW_SCHEMA)
    date = key.strftime('date=%Y-%m-%d')
    path = '/'.join((BUCKET_NAME, SITE_NAME, table_name, date))
    pq.write_to_dataset(data, path, filesystem=fs, compression='gzip')


def read_csv_file(path, fs):
    logger.info('Loading "{}"'.format(path))
    with fs.open(path, 'rb') as f:
        part = pd.read_csv(f, dtype=PANDAS_SCHEMA, parse_dates=[0])
    return part


def main(profile=None, processes=1):
    start_time = time.time()
    fs = s3fs.S3FileSystem(profile_name=profile)
    pool = multiprocessing.Pool(processes=processes)

    logger.info('Getting list of files')
    files = fs.ls(DATA_PATH)
    logger.info('Total number of files {}'.format(len(files)))
    parts = pool.map(partial(read_csv_file, fs=fs), files)
    df = pd.concat(parts)

    load_time = time.time()

    # Exclude rows that have empty temperature
    df = df[pd.notnull(df['temperature'])]

    # Sort values
    df = df.sort_values(by=['timestamp'])

    # Raw data
    raw_data = df.groupby(pd.Grouper(key='timestamp', freq='min'))
    pool.map(partial(write_dataset, fs=fs, table_name='sensor'), raw_data)

    # Downsample minute
    min_data = df.groupby([pd.Grouper(key='timestamp', freq='min'), df.gateway, df.port, df.sensor]).mean().groupby(level=0)
    pool.map(partial(write_dataset, fs=fs, table_name='sensor-minute'), min_data)

    # Downsample hour
    hour_data = df.groupby([pd.Grouper(key='timestamp', freq='H'), df.gateway, df.port, df.sensor]).mean().groupby(level=0)
    pool.map(partial(write_dataset, fs=fs, table_name='sensor-hour'), hour_data)

    loading_time = get_time_delta(load_time, start_time)
    process_time = get_time_delta(time.time(), load_time)
    logger.info('Loading took {}'.format(loading_time))
    logger.info('Process took {}'.format(process_time))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--profile', default=None,
                        help='AWS profile name')
    parser.add_argument('-n', '--processes', type=int, default=1,
                        help='number of pool processes (default is 1)')
    log_group = parser.add_mutually_exclusive_group()
    log_group.add_argument('-d', '--debug', action="store_const",
                           help="print debug info", dest="log_level",
                           const=logging.DEBUG,
                           default=logging.WARNING)
    log_group.add_argument('-v', '--verbose', help="be verbose",
                           action="store_const",
                           dest="log_level", const=logging.INFO)

    args = parser.parse_args()
    args = vars(args)

    log_level = args.pop('log_level')
    logging.basicConfig(
        level=log_level,
        format='[%(levelname)s][%(module)s] %(asctime)s: %(message)s'
    )

    main(**args)
