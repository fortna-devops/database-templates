import argparse
import logging
import os
from datetime import datetime, timezone, timedelta, time as dt_time
from collections import OrderedDict
from io import BytesIO

import boto3
import s3fs
from boto3.dynamodb.conditions import Key

import pandas as pd
import pyarrow as pa
import pyarrow.parquet as pq
import numpy as np

logger = logging.getLogger()


# DHL
# RAW_TABLE = 'Site1'
# LOCATION_TABLE = 'Sensor_Locations'
# ATHENA_TABLE = '"mhspredict-site-dhl-miami"."plc"'
# SITE_ID = '1'

SITE_NAME = 'fedex-louisville'
BUCKET_NAME = 'mhspredict-site-fedex-louisville'
TABLE_NAME = 'plc'
RAW_TABLE = 'PROD-Stack-2-RawTable-BNEFGFPUCGWQ'
LOCATION_TABLE = 'PROD-Stack-2-LocationTable-1DO3ZXZLD8MFJ'
ATHENA_TABLE = '"{}"."{}"'.format(BUCKET_NAME, TABLE_NAME)
RESULT_PATH = 's3://{}/tmp/'.format(BUCKET_NAME)
SITE_ID = '2'


TZ_OFFSET = -4
PAGE_SIZE = 2000

DDB_KEYS = [
    'read_time',
    'gateway',
    'port',
    'sensor',
    'belt_speed',
    'motor_current',
    'motor_freq',
    'motor_voltage',
    'vfd_fault_code',
    'induct_rate',
    'reject_rate',
    'conveyor_status',
]

ATHENA_KEYS = [
    'timestamp',
    'gateway',
    'sensor',
    'port',
    'belt_speed',
    'vfd_current',
    'vfd_frequency',
    'vfd_voltage',
    'vfd_fault_code',
    'sorter_induct_rate',
    'sorter_reject_rate',
    'status'
]

MAPPING = OrderedDict((DDB_KEYS[i], ATHENA_KEYS[i]) for i in range(len(DDB_KEYS)))


DDB_SCHEMA = OrderedDict((
    ('read_time', str),
    ('gateway', str),
    ('port', str),
    ('sensor', str),
    ('belt_speed', np.float64),
    ('motor_current', np.float64),
    ('motor_freq', np.float64),
    ('motor_voltage', np.float64),
    ('vfd_fault_code', np.float64),
    ('induct_rate', np.float64),
    ('reject_rate', np.float64),
    ('conveyor_status', str)
))

PYARROW_SCHEMA = pa.schema([
    pa.field('timestamp', pa.timestamp('ms')),
    pa.field('gateway', pa.string()),
    pa.field('port', pa.string()),
    pa.field('sensor', pa.string()),
    pa.field('belt_speed', pa.int64()),
    pa.field('vfd_current', pa.float64()),
    pa.field('vfd_frequency', pa.float64()),
    pa.field('vfd_voltage', pa.float64()),
    pa.field('vfd_fault_code', pa.int64()),
    pa.field('sorter_induct_rate', pa.int64()),
    pa.field('sorter_reject_rate', pa.int64()),
    pa.field('status', pa.string())
])


def get_result(client, fs, qid):
    while True:  # polling result
        state = client.get_query_execution(
            QueryExecutionId=qid
        )['QueryExecution']['Status']['State']

        if state == 'SUCCEEDED':
            tmp_file = RESULT_PATH + qid + '.csv'

            data = BytesIO()
            with fs.open(tmp_file, 'rb') as fl:
                data.write(fl.read())
            data.seek(0)

            fs.rm(tmp_file)
            fs.rm(tmp_file + '.metadata')
            return data
        if state == 'FAILED':
            raise BaseException('Request to Athena has FAILED')


def get_latest(client, fs):
    query_str = """
    SELECT max(timestamp) as max_timestamp 
    FROM {}
    GROUP BY sensor
    """.format(ATHENA_TABLE)

    query = client.start_query_execution(
        QueryString=query_str,
        ResultConfiguration={
            'OutputLocation': RESULT_PATH
        }
    )

    result = get_result(client, fs, query['QueryExecutionId'])
    latest = pd.read_csv(result, parse_dates=[0]).min()[0].to_pydatetime()
    return latest


def daterange(start_date, end_date):
    for n in range(int((end_date - start_date).days)):
        d = start_date + timedelta(n)
        yield datetime.combine(d, dt_time()).replace(tzinfo=timezone.utc), \
              datetime.combine(d, dt_time(23, 59, 59)).replace(tzinfo=timezone.utc)


def main(profile=None):
    session = boto3.Session(profile_name=profile)
    ddb = session.resource('dynamodb')
    athena = session.client('athena')
    fs = s3fs.S3FileSystem(profile_name=profile)
    paginator = ddb.meta.client.get_paginator('query')

    location_table = ddb.Table(LOCATION_TABLE)
    response = location_table.query(
        KeyConditionExpression=Key('site').eq(SITE_ID)
    )
    equipment = response['Items']

    logger.info('Getting latest date')
    latest_date = get_latest(athena, fs)
    now = datetime.now()
    tz_offset = timezone(timedelta(hours=TZ_OFFSET))

    for start_date, end_date in daterange(latest_date, now):
        logger.info('Getting data from {:%Y-%m-%d %H:%M} to {:%Y-%m-%d %H:%M}'.format(start_date, end_date))
        result = []
        for equip in equipment:
            if 'PLC' not in equip.get('sensor') or equip.get('sensor') == 'ALL_PLC':
                continue

            pages = paginator.paginate(
                TableName=RAW_TABLE,
                KeyConditionExpression=Key('hash_key').eq(equip.get('hash_key')) &
                                       Key('read_time').between(
                                           start_date.astimezone(tz_offset).strftime("%Y-%m-%dT%H:%M%z"),
                                           end_date.astimezone(tz_offset).strftime("%Y-%m-%dT%H:%M%z")
                                       ),
                PaginationConfig={'PageSize': PAGE_SIZE}
            )

            for page in pages:
                items = page['Items']

                sensor_df = pd.DataFrame.from_records(items, columns=DDB_KEYS)
                sensor_df = sensor_df.astype(DDB_SCHEMA)
                sensor_df = sensor_df.rename(columns=MAPPING)
                sensor_df['timestamp'] = pd.to_datetime(sensor_df['timestamp']).astype('datetime64[ms]')
                result.append(sensor_df)

        df = pd.concat(result)
        df = df.sort_values(by=['timestamp'])

        for key, subgroup in df.groupby(pd.Grouper(key='timestamp', freq='min')):
            logger.info('Writing {} file for {:%Y-%m-%d %H:%M}'.format(TABLE_NAME, key))
            data = pa.Table.from_pandas(subgroup, schema=PYARROW_SCHEMA)
            date = key.strftime('date=%Y-%m-%d')
            path = os.path.join('data', SITE_NAME, TABLE_NAME, date)
            # path = '/'.join((BUCKET_NAME, SITE_NAME, table_name, date))
            # pq.write_to_dataset(data, path, filesystem=fs, compression='gzip')
            pq.write_to_dataset(data, path, compression='gzip')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--profile', default=None,
                        help='AWS profile name')
    log_group = parser.add_mutually_exclusive_group()
    log_group.add_argument('-d', '--debug', action="store_const",
                           help="print debug info", dest="log_level",
                           const=logging.DEBUG,
                           default=logging.WARNING)
    log_group.add_argument('-v', '--verbose', help="be verbose",
                           action="store_const",
                           dest="log_level", const=logging.INFO)

    args = parser.parse_args()
    args = vars(args)

    log_level = args.pop('log_level')
    logging.basicConfig(
        level=log_level,
        format='[%(levelname)s][%(module)s] %(asctime)s: %(message)s'
    )

    main(**args)