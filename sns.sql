CREATE EXTERNAL TABLE IF NOT EXISTS `mhspredict-site-fedex-louisville`.alerts (
        sensor string,
        user_id string,
        ack string,
        updated_time string,
        item_data string
        )
        ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
        WITH SERDEPROPERTIES (
            'separatorChar' = ',',
            'quoteChar' = '`',
            'skip.header.line.count' = '1'
        ) LOCATION 's3://mhspredict-site-fedex-louisville/fedex-louisville/alerts'
    TBLPROPERTIES ('has_encrypted_data'='false');