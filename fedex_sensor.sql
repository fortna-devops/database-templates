CREATE EXTERNAL TABLE IF NOT EXISTS `mhs-gg-raw-data`.`sensor_on` (
  `timestamp` TIMESTAMP,
  `gateway` STRING,
  `port` STRING,
  `sensor` STRING,
  `rms_velocity_z` DOUBLE,
  `temperature` DOUBLE,
  `rms_velocity_x` DOUBLE,
  `peak_acceleration_z` DOUBLE,
  `peak_acceleration_x` DOUBLE,
  `peak_frequency_z` DOUBLE,
  `peak_frequency_x` DOUBLE,
  `rms_acceleration_z` DOUBLE,
  `rms_acceleration_x` DOUBLE,
  `kurtosis_z` DOUBLE,
  `kurtosis_x` DOUBLE,
  `crest_acceleration_z` DOUBLE,
  `crest_acceleration_x` DOUBLE,
  `peak_velocity_z` DOUBLE,
  `peak_velocity_x` DOUBLE,
  `hf_rms_acceleration_z` DOUBLE,
  `hf_rms_acceleration_x` DOUBLE
)
         ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe'
         WITH SERDEPROPERTIES (
        'serialization.format' = ',',
                                 'field.delim' = ','
    ) LOCATION 's3://mhs-gg-raw-data/sensor_on/data'
    TBLPROPERTIES ('has_encrypted_data'='false');